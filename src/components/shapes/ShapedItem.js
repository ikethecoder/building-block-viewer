
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

class MultiItemShape {

  build(newNodes, domainType) {
      self = this;
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      var obj = chgSet;//.append("g");//.attr("transform", "translate(" + 80 +","+0+")");


      var offset = {}, maxWidth=100,s = 0, segments = 4, radius = 160, base = 225, range = 90, side = false, delta = {x:140,y:-30}, inner=82

      if ( s== 0) {
        base = 225;
        range = 90;
        offset = { x:320, y:266 };
        maxWidth = 145;
        delta = {x:0,y:0};
      }

      if ( s== 99) {
        base = 45;
        range = 35;//35;
        radius = 150;
        var b0 = self.calculateCoordinate(radius, base + range );
  //console.log("b = "+b0.x+","+b0.y);
        offset = { x:593, y:235 };
        //offset = { x:0, y:0 };
  //console.log("offset = "+offset.x+","+offset.y);
        maxWidth = 50;
        delta = {x:0,y:0} // comes from the SphereSection location of the "Resources" circle
      }

      obj.append("path")
          .attr("stroke-width", "0.05em")
          .attr("stroke", "#8cb7e2")
          .attr("fill", "#white")
          .attr("transform", "translate("+delta.x+","+delta.y+")")
          .attr("d", function(d) {

              if (d.title.segment == "ReactStack") {
                s = 0, segments = 3, side = false, inner=82
              } else if (d.title.segment == "Configuration") {
                s = 2, segments = 3, side = false, inner=82
              } else {
                s = 1, segments = 3, side = false, inner=82
              }
              var loc = self.calculateTilePosition(d.layout.seq, d.layout.total, maxWidth);

              var i = loc.row;
              var dv = loc.col;
              var width = loc.width;
              var iMax = loc.maxRow;

              var c0 = self.calculateCoordinate(radius, base +((0 + (1*(s+((loc.row+1)*(1/(loc.maxRow+0)))))) * range/segments) );
              var x = c0.x;
              var y = c0.y;

              var c1 = self.calculateCoordinate(radius, base +((0 + (1*(s+((loc.row+0)*(1/(loc.maxRow+0)))))) * range/segments) );
              var x1 = c1.x;
              var y1 = c1.y;

              var cx = radius;
              var cy = radius;

              if (dv != 0) {
                  var shift = (dv * (width+(x1-x) + 5));
                  x = x - (dv*(width+0))
                  x1 = x1 - (dv*(width+0));
              }
              var coords = self.focusAt (x,y, x1,y1, x1-width,y1, x-width,y);


              var dat = d3.select(this.parentNode).datum();

              dat.x = offset.x + coords[8];
              dat.y = offset.y + coords[9];

              return "M" + coords[0]+","+coords[1]+" A" + cx + "," + cy + " 0 0,0 " + coords[2] + "," + coords[3] +" L" + coords[4] + "," + coords[5] + " A" + cx + "," + cy + " 0 0,1 " + coords[6] + "," + coords[7] + " L" + coords[0] + "," + coords[1];
          })

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("dx", 0)
          .attr("dy", 0)
          .attr("transform", function(d) {
              if (d.title.segment == "ReactStack") {
                s = 1, segments = 4, radius = 160, side = false, inner=82
              } else {
                s = 2, segments = 4, radius = 160, side = false, inner=82
              }

              var loc = self.calculateTilePosition(d.layout.seq, d.layout.total, maxWidth);

              var i = loc.row;
              var dv = loc.col;
              var width = loc.width;
              var iMax = loc.maxRow;

              var c0 = self.calculateCoordinate(radius, base + ((s+((loc.row+1)*(1/loc.maxRow))) * range/segments) );
              var x = c0.x;
              var y = c0.y;

              var c1 = self.calculateCoordinate(radius, base + ((s+((loc.row)*(1/loc.maxRow))) * range/segments) );
              var x1 = c1.x;
              var y1 = c1.y;

              if (dv != 0) {
                  var shift = (dv * (width+(x1-x) + 5));
                  x = x - (dv*(width+0))
                  x1 = x1 - (dv*(width+0));
              }
              var coords = self.focusAt (x,y, x1,y1, x1-width,y1, x-width,y);
              x = ((coords[0] + coords[2])/2) - width/2;
              y = coords[1] + (coords[3]-coords[1])/2;

              return "translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") scale(1,1)"
          })
          .text(function(d) { return d.title.solution; });

  }

  calculateDims () {
  }

  focusAt (x1,y1,x2,y2,x3,y3,x4,y4) {
      var coords = []
      var lowPoint = {x:x3, y:y3 }
      coords.push(x1 - lowPoint.x);
      coords.push(y1 - lowPoint.y);
      coords.push(x2 - lowPoint.x);
      coords.push(y2 - lowPoint.y);
      coords.push(x3 - lowPoint.x);
      coords.push(y3 - lowPoint.y);
      coords.push(x4 - lowPoint.x);
      coords.push(y4 - lowPoint.y);
      coords.push(lowPoint.x)
      coords.push(lowPoint.y)
      return coords;
  }

  calculateCoordinate (radius, angle) {
      console.log("CalculateCoordinates : " + radius + " : " + angle);
      var A = (angle - 90) * (Math.PI/180);
      var x = (radius) * Math.cos(A) ;
      var y = (radius) * Math.sin(A);
      return {x:x, y:y}
  }

  calculateTilePosition (tileNumber, tileCount, maxWidth) {
      var iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
      if (iMax == tileCount) {
          tileCount++;
          iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
          tileCount--;
      }
      var dvMax = Math.ceil(tileCount / iMax)
      var tile = tileCount - 1;

      var width = maxWidth/dvMax;//Math.round((baseX - (extended - delta.x)) / iMax);

      for (var i = 0; i < iMax; i++) {

          for (var dv = 0; dv < dvMax; dv++) {
              if (tile == tileNumber) {
                  return {row:i, col:dv, width:maxWidth/dvMax, maxRow:iMax, maxCol:dvMax};
              }
              tile--;
          }
      }
      alert("Tile not found!");
  }
}

export default MultiItemShape;


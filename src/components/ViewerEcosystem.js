import React, {PropTypes} from 'react';

import $ from 'jquery';

import ReactDOM from 'react-dom';

import ViewerBox from './ViewerBox.js';
import ViewerCanvas from './ViewerCanvas.js';

class Viewer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.save = this.save.bind(this);
    this.onTimeframeChange = this.onTimeframeChange.bind(this);
    this.fuelSavingsKeypress = this.fuelSavingsKeypress.bind(this);
  }

  fuelSavingsKeypress(name, value) {
    this.props.calculateFuelSavings(this.props.appState, name, value);
  }

  save() {
    //this.props.saveFuelSavings(this.props.appState);
      const obj = ReactDOM.findDOMNode(this);

      const width = obj.offsetWidth;
      console.log("Width: " + width);

  }

  componentDidMount() {
      const obj = ReactDOM.findDOMNode(this);

      console.log("id = "+obj.id);
      const width = obj.offsetWidth;
      console.log("Width: " + width);

      //this.loadDataFromServer();

      var el = document.createElement( 'i' );
      $(el).addClass("fa fa-home");

      //el.getElementsByTagName( 'a' );

      //var el = $(document.body).append( '<i class="fa fa-home"></i>' );
      //var el = $(document.body).append( '<i class="fa fa-home"></i>' );
      console.log("Result: " + $(el).html().length);

      const FA_GROUP = "&#xf0c0";
      const g = {
        "nodes":
[
//  {
//    "name": "Background",
//    "title": {
//      "label": "Live",
//      "pipeline": true,
//      "order": {
//        "product": "I2",
//        "qty": 1
//      }
//    },
//    "x": 400,
//    "y": 260,
//    "type": "multiItemSection",
//    "fixed": true,
//    "boundary": "A"
//  },
  {
    "name": "PE",
    "title": {
      "icon": "&#xf0ad",
      "label": "Build"
    },
    "x": 115,
    "y": 115,
    "type": "lifecycle",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "MEASURE",
    "title": {
      "icon": "&#xf0e4",
      "label": "Measure"
    },
    "x": 690,
    "y": 460,
    "type": "lifecycle",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "LEARN",
    "title": {
      "icon": "&#xf19c",
      "label": "Learn"
    },
    "x": 115,
    "y": 460,
    "type": "lifecycle",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "DO",
    "title": {
      "segment": "Infrastructure",
      "role": "Provisioning",
      "solution": "Digital Ocean"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "OS",
    "title": {
      "segment": "Runtime",
      "role": "Operating System",
      "solution": "CentOS"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "GL",
    "title": {
      "segment": "Develop",
      "role": "Source Control",
      "solution": "GitLab"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CD",
    "title": {
      "segment": "Deploy",
      "role": "Delivery Pipeline",
      "solution": "Go.CD"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "RC",
    "title": {
      "segment": "Develop",
      "role": "Collaboration",
      "solution": "Rocket.Chat"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "DK",
    "title": {
      "segment": "Runtime",
      "role": "Container Engine",
      "solution": "Docker"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "ND",
    "title": {
      "segment": "Runtime",
      "role": "Virtual Machine",
      "solution": "NodeJS"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JV",
    "title": {
      "segment": "Runtime",
      "role": "Virtual Machine",
      "solution": "Java"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "DKR",
    "title": {
      "segment": "Registry",
      "role": "Artifact Registry",
      "solution": "Docker"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JVR",
    "title": {
      "segment": "Registry",
      "role": "Artifact Registry",
      "solution": "Archiva"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "AN",
    "title": {
      "segment": "Deploy",
      "role": "Deployment",
      "solution": "Ansible"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "SE",
    "title": {
      "segment": "Monitoring",
      "role": "Ops Alerting",
      "solution": "Sensu"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "ELK",
    "title": {
      "segment": "Monitoring",
      "role": "Ops Monitoring",
      "solution": "Elastic.io"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CN",
    "title": {
      "segment": "Config",
      "role": "Configuration Mgmt",
      "solution": "Hashicorp Consul"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "KG",
    "title": {
      "segment": "Config",
      "role": "API Management",
      "solution": "Kong"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "VA",
    "title": {
      "segment": "Security",
      "role": "Key Management",
      "solution": "Hashicorp Vault"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "LETS",
    "title": {
      "segment": "Security",
      "role": "SSL Cert Provider",
      "solution": "LetsEncrypt"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "VA",
    "title": {
      "segment": "Security",
      "role": "Virtual Private Network",
      "solution": "Strongswan"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "NG",
    "title": {
      "segment": "Config",
      "role": "Load Balancing",
      "solution": "NGINX"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "MG",
    "title": {
      "segment": "Database",
      "role": "Document Database",
      "solution": "MongoDB"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "RAB",
    "title": {
      "segment": "Database",
      "role": "Messaging",
      "solution": "RabbitMQ"
    },
    "type": "shapedItemVertical",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA01",
    "title": {
      "segment": "ReactStack",
      "solution": "React"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA02",
    "title": {
      "segment": "ReactStack",
      "solution": "Redux"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA03",
    "title": {
      "segment": "ReactStack",
      "solution": "Babel"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA04",
    "title": {
      "segment": "ReactStack",
      "solution": "Webpack"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA05",
    "title": {
      "segment": "ReactStack",
      "solution": "Browsersync"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA06",
    "title": {
      "segment": "ReactStack",
      "solution": "Mocha"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA07",
    "title": {
      "segment": "ReactStack",
      "solution": "Isparta"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA08",
    "title": {
      "segment": "ReactStack",
      "solution": "TrackJS"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA09",
    "title": {
      "segment": "ReactStack",
      "solution": "ESLint"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA10",
    "title": {
      "segment": "ReactStack",
      "solution": "SASS"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA11",
    "title": {
      "segment": "ReactStack",
      "solution": "npm"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CONF01",
    "title": {
      "segment": "Configuration",
      "solution": "Ruby"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CONF02",
    "title": {
      "segment": "Configuration",
      "solution": "Python"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CONF03",
    "title": {
      "segment": "Configuration",
      "solution": "Groovy"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV01",
    "title": {
      "segment": "JavaStack",
      "solution": "Java"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV02",
    "title": {
      "segment": "JavaStack",
      "solution": "Spring"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV03",
    "title": {
      "segment": "JavaStack",
      "solution": "Selenium"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV04",
    "title": {
      "segment": "JavaStack",
      "solution": "Maven"
    },
    "type": "shapedItem",
    "fixed": true,
    "boundary": "A"
  },
//  {
//    "name": "TA",
//    "title": {
//      "role": "Test Automation",
//      "solution": ""
//    },
//    "x": 570,
//    "y": 108,
//    "type": "buildingBlockMicro",
//    "fixed": true,
//    "boundary": "A"
//  },
//  {
//    "name": "TA01",
//    "title": {
//      "solution": "Accessibility"
//    },
//    "x": 767,
//    "y": 116,
//    "type": "stackSolutionMicro",
//    "fixed": true,
//    "boundary": "A"
//  },
//  {
//    "name": "TA02",
//    "title": {
//      "solution": "Functional"
//    },
//    "x": 767,
//    "y": 98,
//    "type": "stackSolutionMicro",
//    "fixed": true,
//    "boundary": "A"
//  },
//  {
//    "name": "TA03",
//    "title": {
//      "solution": "Stress"
//    },
//    "x": 725,
//    "y": 115,
//    "type": "stackSolutionMicro",
//    "fixed": true,
//    "boundary": "A"
//  },
//  {
//    "name": "TA04",
//    "title": {
//      "solution": "Load"
//    },
//    "x": 683,
//    "y": 99,
//    "type": "stackSolutionMicro",
//    "fixed": true,
//    "boundary": "A"
//  },
//  {
//    "name": "TA05",
//    "title": {
//      "solution": "Soak"
//    },
//    "x": 725,
//    "y": 99,
//    "type": "stackSolutionMicro",
//    "fixed": true,
//    "boundary": "A"
//  },
//  {
//    "name": "TA06",
//    "title": {
//      "solution": "Spike"
//    },
//    "x": 683,
//    "y": 116,
//    "type": "stackSolutionMicro",
//    "fixed": true,
//    "boundary": "A"
//  },
  {
    "name": "LOGO",
    "title": {
      "image": "images/canzea-managedby.svg",
      "width": 140,
      "height": 20.95295980409741
    },
    "x": -5,
    "y": 15,
    "type": "image",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvBuild",
    "title": {
      "label": "Build",
      "pipeline": false,
      "order": {
        "product": "I4",
        "qty": 1
      }
    },
    "x": 130,
    "y": 72,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvPerf",
    "title": {
      "label": "Performance",
      "pipeline": false,
      "order": {
        "product": "I2",
        "qty": 10
      }
    },
    "x": 240,
    "y": 72,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvInt",
    "title": {
      "label": "Integration",
      "pipeline": true,
      "order": {
        "product": "I1",
        "qty": 1
      }
    },
    "x": 360,
    "y": 72,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvUser",
    "title": {
      "label": "User Acceptance",
      "pipeline": true,
      "order": {
        "product": "I2",
        "qty": 1
      }
    },
    "x": 465,
    "y": 72,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvLive",
    "title": {
      "label": "Live",
      "pipeline": true,
      "order": {
        "product": "I2",
        "qty": 1
      }
    },
    "x": 570,
    "y": 72,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "ZoneA",
    "title": {
      "label": "A"
    },
    "x": 730,
    "y": 125,
    "type": "zone",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "Resource_Source",
    "title": {
      "role": "",
      "solution":"Source"
    },
    "x": 760,
    "y": 450,
    "type": "stackedBuildingBlockMicro",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "Resource_Data",
    "title": {
      "role": "",
      "solution":"Data"
    },
    "x": 760,
    "y": 360,
    "type": "stackedBuildingBlockMicro",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "Resource_Files",
    "title": {
      "role": "",
      "solution":"Files"
    },
    "x": 760,
    "y": 390,
    "type": "stackedBuildingBlockMicro",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "Resource_External",
    "title": {
      "role": "",
      "solution":"External"
    },
    "x": 760,
    "y": 420,
    "type": "stackedBuildingBlockMicro",
    "fixed": true,
    "boundary": "A"
  }
]
        ,
        "links": [
        ]
      }

      const links = [
//        {"source":"CANZEA", "target":"DO", "type":"straight"},
//        {"source":"CANZEA", "target":"GL", "type":"straight"},
//        {"source":"GL", "target":"CD", "type":"straight"},
//        {"source":"PE", "target":"GL", "type":"straight", "label": "Pushes code"},
//        {"source":"PE", "target":"RC", "type":"straight"},
//        {"source":"CD", "target":"MV", "type":"straight"},
//        {"source":"MV", "target":"DK", "type":"straight"},
//        {"source":"MV", "target":"JV", "type":"straight"},
//        {"source":"JV", "target":"JVR", "type":"straight"},
//        {"source":"DK", "target":"DKR", "type":"straight"},
//        {"source":"CD", "target":"AN", "type":"straight"},
//        {"source":"CD", "target":"DO", "type":"straight"},
//        {"source":"CD", "target":"TA", "type":"straight"},
//        {"source":"SE", "target":"RC", "type":"straight"},
//        {"source":"PE", "target":"ELK", "type":"straight"},
//        {"source":"DO", "target":"OS", "type":"straight"}

//        {"source":"Digital", "target":"IDE", "type":""},
////        {"source":"Digital", "target":"UTE", "type":""},
////        {"source":"Digital", "target":"Perf", "type":""},
////        {"source":"Digital", "target":"Live", "type":""},
//        {"source":"App-1", "target":"MongoDB", "type":""},
//        {"source":"App-1", "target":"Go.CD Agent_2", "type":""},
//        {"source":"App-1", "target":"etcd_2", "type":""},
//        {"source":"App-1", "target":"Kong", "type":""},
//        {"source":"App-1", "target":"NGINX", "type":""},
//        {"source":"CD", "target":"GitLab", "type":""},
//        {"source":"CD", "target":"etcd", "type":""},
//        {"source":"CD", "target":"Go.CD", "type":""},
//        {"source":"CD", "target":"Ansible", "type":""},
//        {"source":"CD", "target":"Go.CD Agent", "type":""},
//        {"source":"etcd", "target":"do-sf-1", "type":""},
//        {"source":"etcd", "target":"do-sf-2", "type":""},
//        {"source":"etcd", "target":"do-sf-4", "type":""},
//        {"source":"etcd", "target":"do-sf-5", "type":""},
//        {"source":"etcd", "target":"do-sf-6", "type":""},
//        {"source":"etcd", "target":"do-sf-7", "type":""},
//        {"source":"etcd", "target":"do-sf-8", "type":""},
//        {"source":"etcd", "target":"do-sf-9", "type":""},
//        {"source":"etcd", "target":"do-sf-10", "type":""},
//        {"source":"etcd", "target":"do-sf-3", "type":""}
      ];

      this.refs.canvas.setState({graph: g, links: links});
      this.setState({graph: g, links: links});

      const self = this;
      $(window).resize(function (e) {
          self.refs.canvas.refreshSize();
      });

  }

  onTimeframeChange(e) {
    //this.props.calculateFuelSavings(this.props.appState, 'milesDrivenTimeframe', e.target.value);
  }


  loadDataFromServer() {
      let self = this;

      const eid = '572f9954d4c67f8e5573d7dd';

      console.log("loading data from server...");
      const url = "http://localhost:8888/api/saasexpress/platform/" + eid;
      let request = $.get(url, function (response) {

          let links = response.links;
          response.links = [];

          //self.graph = response;
          //self.refs.canvas.setGraph(response);
          //self.setState(response);
          //self.recalcSpace();
          self.refs.canvas.setState({"graph":response, "links":links});
          //self.refs.canvas.update();
//
//          if (links) {
//              for (var i = 0 ; i < links.length; i++) {
//                  self.refs.canvas.addLink(links[i].source, links[i].target, links[i].type);
//              }
//
//          }
      });
  }

//      <div style={{width:"9933px",height:"7017px"}} className="canvas">

  render() {
    let ratio = 0.70643; // A1 ratio
    ratio = 0.666666; // 24x36 poster
    let w = 800;
    let h = (w * ratio);

    let margin = 10;

    const zones = [
        { "type" : "swimlane", "rectangle": [margin, margin, w - (2*margin), h - (2*margin)], "title": "" },
        { "type" : "sectionTop", "rectangle": [340, 360, 200, 160], "title": "Top" },
        { "type" : "sectionBottom", "rectangle": [340, 360, 200, 160], "title": "Bottom" },
        { "type" : "sectionCenter", "rectangle": [340, 360, 200, 160], "title": "Center" },
        { "type" : "sectionLeft", "rectangle": [10, 200, 200, 160], "title": "Left" },
        { "type" : "sectionRight", "rectangle": [340, 360, 200, 160], "title": "Right" },
        { "type" : "verticalLabel", "layout": {"seg":"Security"}, "label":"Security" },
        { "type" : "verticalLabel", "layout": {"seg":"Database"}, "label":"Data" },
        { "type" : "verticalLabel", "layout": {"seg":"Config"}, "label":"Servicing" },
        { "type" : "verticalLabel", "layout": {"seg":"Runtime"}, "label":"Runtime" },
        { "type" : "verticalLabel", "layout": {"seg":"Monitoring"}, "label":"Monitoring" }
//        { "type" : "multiItemSection", "rectangle": [340, 360, 200, 160], "title": "Java Stack" }
//        { "type" : "applicationStack", "rectangle": [340, 360, 200, 160], "title": "Application Stack" },
//        { "type" : "applicationStack", "rectangle": [550, 360, 95, 160], "title": "Applications" }
//        { "type" : "segment", "rectangle": [650, 85, 135, 100], "title": "Testing Stack" },
//        { "type" : "segment", "rectangle": [15, 360, 130, 160], "title": "Browser Stack" },
//        { "type" : "segment", "rectangle": [146, 360, 80, 160], "title": "Java Stack" },
//        { "type" : "securityStack", "rectangle": [235, 360, 100, 160], "title": "Security" }
        //{ "type" : "environment", "rectangle": [10, 300, 600, 160], "title": "Environments" }
    ];

    return (
      <div className="canvas">
        <ViewerCanvas ref="canvas" zones={zones} graph="{this.graph}"/>
      </div>
    );
  }
          /*
          <pre style={{display:"none"}}>{JSON.stringify(this.state, null, 2)}</pre>

          <div style={{padding:"10px"}}>
            <img src="images/canzea_logo.svg" style={{width:"400px"}}/>
          </div>

          <input type="submit" value="Save" onClick={this.save}/>
           */
}


Viewer.propTypes = {
};

export default Viewer;

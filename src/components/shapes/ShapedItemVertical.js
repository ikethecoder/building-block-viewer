
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

class MultiItemShape {

  build(newNodes, domainType) {
      self = this;
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      var obj = chgSet;//.append("g");//.attr("transform", "translate(" + 80 +","+0+")");

      var coords, offset = {x:402,y:266} // center point for the ecosystem

      //var radius = 160, base = 0, range = 180, height = 50, radiusZeroPoint = 0, rotate = (base + (range/2) - 90);

// Needed for case of "Application" and "Resources"
//      offset.x += 130
//      offset.y += -30

//      var deg = Math.atan2(-c3.y + c4.y, -c3.x + c4.x)*180.0/Math.PI;
//      var o3 = self.calculateX (radius + height, c4.y, deg);
//
//      var deg2 = Math.atan2(c2.y-c1.y, c2.x - c1.x)*180.0/Math.PI;
//      var o2 = self.calculateX (radius + height, c1.y, deg2);

//alert("c2 = "+c2.x+","+o2.x);
//      coords = self.focusAt(c1.x,c1.y, o2.x,o2.y, o3.x,o3.y, c4.x,c4.y);

//      var _y = c4.y;
//      var _radius = radius + height;
//      var _angle = 0; // unknown?
//      var _x = 0; // unknown?
//      var _A = (_angle - 90) * (Math.PI/180);
//      var deltaX = 0;
//      var deltaY = 0;
//      var _deg = Math.atan2(deltaY, deltaX)*180.0/Math.PI;
//      _radius = _x/Math.cos(_A);// = _y/Math.cos(_A);
//
//      var _degA = Math.atan2(c3.y - b0.y, c3.x - b0.x)*180.0/Math.PI;
      //var _degA + ? =
//      var A = (angle - 90) * (Math.PI/180);
//      var x = (radius) * Math.cos(A) ;
//      var y = (radius) * Math.sin(A);

      var radius = 500;
      var height = 20;

      function doit(info) {
          var seg, top;
          if (info.seg == "Monitoring") { seg = 4; top = false;}
          if (info.seg == "Database") { seg = 3; top = false;}
          if (info.seg == "Config") { seg = 2; top = false; }
          if (info.seg == "Security") { seg = 0; top = false; }

          if (info.seg == "Develop") { seg = 1; top = true; }
          if (info.seg == "Deploy") { seg = 2; top = true; }
          if (info.seg == "Runtime") { seg = 1; top = false; }
          if (info.seg == "Registry") { seg = 4; top = true; }
          if (info.seg == "Infrastructure") { seg = 5; top = true; }

          var radius,base,range,height,radiusZeroPoint,padding;

          if (top) {
              radius = 500 + (info.row*27), base = -20, range = 5, height = 25, radiusZeroPoint = 425, padding=.5;
          } else {
              radius = 500 + (info.row*27), base = 160, range = 6, height = 25, radiusZeroPoint = 392, padding=2;
              //radius = 160, base = 225, range = 90, height = 25, radiusZeroPoint = 0, padding=2;

          }

          var col = (info.col*range) + (seg*(range+padding));
          var b0 = self.calculateCoordinate(radiusZeroPoint, base);
          var c1 = self.calculateCoordinate(radius, base + col);
          var c2 = self.calculateCoordinate(radius + height , base + col);
          var c3 = self.calculateCoordinate(radius + height, base + range + col);
          var c4 = self.calculateCoordinate(radius, base + range + col);

          var center = self.calculateCoordinate(radius + (height/2) , base + (range/2) + col);

          offset = {x:402,y:266}
          offset.y -=  b0.y; // adjust the Y axis to the zero point of the sphere


          var distance = Math.abs(Math.sqrt(Math.pow(c3.x, 2) + Math.pow(c3.y, 2)));

          // Keep the x axis fixed
          var _y3 = Math.sqrt(Math.pow(distance,2) - Math.pow(c4.x, 2));
          var o3 = {x:c4.x, y:_y3}

          var _y2 = Math.sqrt(Math.pow(distance,2) - Math.pow(c1.x, 2));
          var o2 = {x:c1.x, y:_y2}

          // Keep the y axis fixed
    //      var _x3 = Math.sqrt(Math.pow(distance,2) - Math.pow(c4.y, 2));
    //      var o3 = {x:_x3, y:c4.y}
    //
    //      var _x2 = Math.sqrt(Math.pow(distance,2) - Math.pow(c1.y, 2));
    //      var o2 = {x:_x2, y:c1.y}

          //var resp = self.focusAt(c1.x,c1.y, c1.x,c2.y, c4.x,c3.y, c4.x,c4.y);
          var resp = self.focusAt(c1.x,c1.y, c2.x,c2.y, c3.x,c3.y, c4.x,c4.y);
          resp.push(center.x);
          resp.push(center.y);
          resp.push(base + (range/2) + col + (top ? 0:-180));
          return resp;
          //coords = self.focusAt(c1.x,c1.y, c2.x,c2.y, c3.x,c3.y, c4.x,c4.y);
      }

//      obj.append("circle").attr("fill","blue").attr("r", 5).attr("transform", "translate(" + c1.x+","+c1.y+")");
//      obj.append("circle").attr("fill","blue").attr("r", 10).attr("transform", "translate(" + c2.x+","+c2.y+")");
//      obj.append("circle").attr("fill","blue").attr("r", 15).attr("transform", "translate(" + c3.x+","+c3.y+")");
//      obj.append("circle").attr("fill","blue").attr("r", 20).attr("transform", "translate(" + c4.x+","+c4.y+")");

      obj.append("path")
          .attr("stroke-width", "0.05em")
          .attr("stroke", "#CCCCCC")
          .attr("fill", "#white")
          .attr("d", function(d) {

              coords = doit(self.calculateTilePosition(d.layout.seq, d.layout.total, d.layout.seg));

              var dat = d3.select(this.parentNode).datum();
              dat.x = coords[8] + offset.x;
              dat.y = coords[9] + offset.y;

              return "M" + coords[0]+","+coords[1]+" L" + coords[2] + "," + coords[3] +" A" + (radius + height) + "," + (radius + height) + " 0 0,1 " + coords[4] + "," + coords[5] + " L" + coords[6] + "," + coords[7] + " A" + radius + "," + radius + " 0 0,0 " + coords[0] + "," + coords[1];
          });

      obj.append("text")
          .attr("class", "solution")
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("dx", 0)
          .attr("dy", -4)
          .attr("transform", function(d) {


              coords = doit(self.calculateTilePosition(d.layout.seq, d.layout.total, d.layout.seg));

              var rotate = coords[12];

              var dat = d3.select(this.parentNode).datum();
              dat.x = coords[8] + offset.x;
              dat.y = coords[9] + offset.y;

              return "translate(" + (coords[10] - coords[8]) + "," + (coords[11] - coords[9]) +") rotate(" + rotate + ")";

          })
          .text(function(d) { return d.title.solution; });

      obj.append("text")
          .attr("class", "role")
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("dx", 0)
          .attr("dy", 4)
          .attr("transform", function(d) {


              coords = doit(self.calculateTilePosition(d.layout.seq, d.layout.total, d.layout.seg));

              var rotate = coords[12];

              var dat = d3.select(this.parentNode).datum();
              dat.x = coords[8] + offset.x;
              dat.y = coords[9] + offset.y;

              return "translate(" + (coords[10] - coords[8]) + "," + (coords[11] - coords[9]) +") rotate(" + rotate + ")";

          })
          .text(function(d) { return d.title.role; });
  }

  focusAt (x1,y1,x2,y2,x3,y3,x4,y4) {
      var coords = []
      var lowPoint = {x:x3, y:y3 }
      coords.push(x1 - lowPoint.x);
      coords.push(y1 - lowPoint.y);
      coords.push(x2 - lowPoint.x);
      coords.push(y2 - lowPoint.y);
      coords.push(x3 - lowPoint.x);
      coords.push(y3 - lowPoint.y);
      coords.push(x4 - lowPoint.x);
      coords.push(y4 - lowPoint.y);
      coords.push(lowPoint.x)
      coords.push(lowPoint.y)
      return coords;
  }

  calculateCoordinate (radius, angle) {
      console.log("CalculateCoordinates : " + radius + " : " + angle);
      var A = (angle - 90) * (Math.PI/180);
      var x = (radius) * Math.cos(A) ;
      var y = (radius) * Math.sin(A);
      return {x:x, y:y}
  }

  calculateX (radius, y, deg) {
      console.log("CalculateCoordinates : " + radius + " : " + deg);
      var A = (deg) * (Math.PI/180);
      //var x = (radius) * Math.cos(A) ; // radius = x/Math.cos(A) = y/Math.sin(A) -> y = x*Math.sin(A)/Math.cos(A)
      var x = y * Math.cos(A) / Math.sin(A);
      return {x:x, y:y}
  }

  calculateTilePosition (tileNumber, tileCount, segment) {
      var iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
      if (iMax == tileCount) {
          tileCount++;
          iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
          tileCount--;
      }
      var dvMax = Math.ceil(tileCount / iMax)
      var tile = tileCount - 1;

      var width = 145/dvMax;//Math.round((baseX - (extended - delta.x)) / iMax);

      iMax = tileCount;
      dvMax = 1;
      for (var i = 0; i < iMax; i++) {

          for (var dv = 0; dv < dvMax; dv++) {
              if (tile == tileNumber) {
                  return {row:i, col:dv, width:145/dvMax, maxRow:iMax, maxCol:dvMax, seg:segment};
              }
              tile--;
          }
      }
      alert("Tile not found!");
  }
}

export default MultiItemShape;



import d3 from 'd3';
import React, {PropTypes} from 'react';

class ZoneShape {

  build(newNodes, domainType) {
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      var c0 = this.calcRectangle(88, 148);

      chgSet.append("rect")
          .attr("class", "bg")
          .attr("x", c0.x)
          .attr("y", c0.y)
          .attr("width", c0.w)
          .attr("height", c0.h)

      var c1 = this.calcRectangle(80, 140);
      chgSet.append("rect")
          .attr("x", c1.x)
          .attr("y", c1.y)
          .attr("width", c1.w)
          .attr("height", c1.h)
          //.style("filter", "url(#drop-shadow)")

      chgSet.append("text")
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("dx", 0)
          .attr("dy", 5 -c1.h/2)
          .text(function(d) { return d.name; });

//      var c2 = this.calcRectangle(70, 50);
//      chgSet.append("rect")
//          .attr("x", c2.x)
//          .attr("y", c2.y)
//          .attr("width", c2.w)
//          .attr("height", c2.h)

       //   <image x="10" y="20" width="80" height="80" xlink:href="recursion.svg" />
//      chgSet.append("image")
//          .attr("x", -60)
//          .attr("y", -20)
//          .attr("width", 120)
//          .attr("height", 120 * (66/312))
//          .attr("xlink:href", "images/canzea_logo.svg");

  }

  calcRectangle (w, h) {
      return { x: -w/2, y: -h/2, w:w, h:h }
  }
}

export default ZoneShape;


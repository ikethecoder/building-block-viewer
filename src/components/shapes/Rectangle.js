
import d3 from 'd3';
import React, {PropTypes} from 'react';

class RectangleShape {

  build(newNodes, domainType) {
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      chgSet.append("rect")
          .attr("x", -75)
          .attr("y", -40)
          .attr("width", 150)
          .attr("height", 80)
          //.style("filter", "url(#drop-shadow)")

      chgSet.append("text")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 30)
          .text(function(d) { return d.name; });


       //   <image x="10" y="20" width="80" height="80" xlink:href="recursion.svg" />
//      chgSet.append("image")
//          .attr("x", -60)
//          .attr("y", -20)
//          .attr("width", 120)
//          .attr("height", 120 * (66/312))
//          .attr("xlink:href", "images/canzea_logo.svg");

  }
}

export default RectangleShape;


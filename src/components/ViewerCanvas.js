import React, {PropTypes} from 'react';
import ReactDOM from 'react-dom';

import $ from 'jquery';
import d3 from 'd3';
import HexShape from './shapes/Hex.js';
import RectangleShape from './shapes/Rectangle.js';
import BuildingBlockShape from './shapes/BuildingBlock.js';
import BuildingBlockMicroShape from './shapes/BuildingBlockMicro.js';
import CircleShape from './shapes/Circle.js';
import IconShape from './shapes/Icon.js';
import MiniCircleShape from './shapes/MiniCircle.js';
import CommentShape from './shapes/Comment.js';
import Navigation from './NavigationHorizontal.js';
import ImageShape from './shapes/Image.js';
import TableShape from './shapes/Table.js';
import LifecycleShape from './shapes/Lifecycle.js';
import ZoneShape from './shapes/Zone.js';
import SectionLeft from './shapes/SectionLeft.js';
import SectionRight from './shapes/SectionRight.js';
import SectionTop from './shapes/SectionTop.js';
import SectionBottom from './shapes/SectionBottom.js';
import MultiItemShape from './shapes/MultiItemSection.js';
import ShapedItem from './shapes/ShapedItem.js';
import ShapedItemVerticalLabel from './structure/ShapedItemVerticalLeftLabel.js';
import SectionCenter from './structure/SectionCenter.js';
import ShapedItemVertical from './shapes/ShapedItemVerticalLeft.js';
import StackedBuildingBlockShape from './shapes/StackedBuildingBlock.js';

class ViewerCanvas extends React.Component {
  constructor(props, context) {
    super(props, context);

  }

  componentDidMount() {
      const svgObject = ReactDOM.findDOMNode(this);

//      const width = svgObject.parentNode.offsetWidth;
//      console.log("ComponentDid Mount :: Width: " + width);

      this.initCanvas(svgObject);
  }

  initCanvas(el) {
      // set up the D3 visualisation in the specified element

      //const positionInfo = el.parentNode.getBoundingClientRect();

      //let w = positionInfo.width,
      //    h = positionInfo.height;

      let self = this;

      let ratio = 0.70643; // A1 ratio
      ratio = 0.666666; // 24x36 poster
      let w = 800;
      let h = w * ratio;
      this.zones = this.props.zones;

      const ow = $(el).parent().innerWidth();
      let oh = $(el).parent().innerHeight();

      console.log("W = "+ow +", H = " + oh);

      // maintain a 1/1 ratio tied to width
      oh = ow;
      let vis = d3.select(el)
          .attr("width", ow)
          .attr("height", ow * ratio);

      let rect;// = this.zones[2].rectangle;

      rect = [0, 0, w, h];

      const zone = "" + (rect[0]) + " " + rect[1] + " " + rect[2] + " " + rect[3];

      vis.attr("preserveAspectRatio", "xMinYMin")
      vis.attr("viewBox", zone); // "0 0 1000 800"

      const defs = vis.append("defs");
      defs.selectAll("marker")
          .data(["suit", "licensing", "resolved", "lifecycle"])
        .enter().append("marker")
          .attr("id", function(d) { return d; })
          .attr("class", function(d) { return "_end_" + d; })
          .attr("viewBox", "0 -5 10 10")
          .attr("refX", 0)
          .attr("refY", 0)
          .attr("markerWidth", 6)
          .attr("markerHeight", 6)
          .attr("orient", "auto")
        .append("path")
          .attr("d", "M0,-5L10,0L0,5");

      const filter = defs.append("filter").attr("id", "drop-shadow").attr("x", 0).attr("y", 0).attr("height", "150%").attr("width", "150%");
      filter.append("feOffset").attr("in", "SourceAlpha").attr("dx", 3).attr("dy", 3).attr("result", "offOut");
      filter.append("feGaussianBlur").attr("in", "offOut").attr("stdDeviation", 1).attr("result", "blurOut");
      filter.append("feBlend").attr("in", "SourceGraphic").attr("in2", "blurOut").attr("mode", "normal");
//      const feMerge = filter.append("feMerge");
//      feMerge.append("feMergeNode").attr("in", "offsetBlur");
//      feMerge.append("feMergeNode").attr("in", "SourceGraphic");


      vis.append("rect")
          .style("fill", "white")
          .attr("x", 0)
          .attr("y", 0);


      this.zones.forEach(function (zone) {
        if (zone.type == "sectionLeft") {
          new SectionLeft().build(vis, "underlay");
        } else if (zone.type == "sectionRight") {
          new SectionRight().build(vis, "underlay");
        } else if (zone.type == "sectionTop") {
          new SectionTop().build(vis, "underlay");
        } else if (zone.type == "sectionBottom") {
          new SectionBottom().build(vis, "underlay");
        } else if (zone.type == "sectionCenter") {
          new SectionCenter().build(vis, "underlay");
        } else if (zone.type == "verticalLabel") {
          new ShapedItemVerticalLabel().build(vis, "verticalLabel", zone);

        } else {
          vis.append("rect")
              //.style("filter", "url(#drop-shadow)")
              .attr("x", zone.rectangle[0])
              .attr("y", zone.rectangle[1])
              .attr("width", zone.rectangle[2])
              .attr("height", zone.rectangle[3])
              .attr("class", zone.type); //fff899
          vis.append("text")
            .attr("class", "title")
            .attr("dx", zone.rectangle[0] + 5)
            .attr("dy", zone.rectangle[1] + 10)
            .text(zone.title);
         }
      });

      vis.append("g").attr("class", "all_lines");

      vis.append("g").attr("class", "nodeSet");

      const calcLinkDistance = function (a, b) {
//          return Math.abs(Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)));

         if (a.source.boundary != a.target.boundary) {
            return w/4;
         } else if (a.type == "straight") {
            console.log(Math.sqrt(((a.source.x - a.target.x) * 2) + ((a.source.y - a.target.y) * 2)));
            return 250;
         } else {
            return 80;
         }
      }

      let force = d3.layout.force()
          .links([])
          .nodes([])
          .gravity(0)
          .charge(0)
          .linkDistance(calcLinkDistance)
          .size([w, h]);

      this.vis = vis;
      this.force = force;

      this.navigation = new Navigation();

      this.navigation.attach(vis);

      this.vis.on("click", function (d) {
         //self.navigation.close();
      });
  }

  refreshSize() {
      const w = $(this.vis.node()).parent().innerWidth();
      this.vis.attr("width", w);
      this.vis.attr("height", w);
  }

  getDim(d) {
      const index = d.boundary.charCodeAt(0) - 65;
      if (this.zones.length <= index) {
          console.log("INVALID BOUNDARY: " + d.boundary);
      }
      const rect = this.zones[index].rectangle;
      const dim = {x1:rect[0],y1:rect[1],x2:rect[0] + rect[2],y2:rect[1] + rect[3]};
      //console.log("K: " + JSON.stringify(dim, null, 2));
      return dim;
  }

  clear() {
      console.log("CLEARING");
      this.vis.select("g.nodeSet").selectAll("g.node").remove();
  }

  update() {
      console.log("Update...");

      const self = this;
      const vis = this.vis;

      let graph = this.state.graph;
      const force = this.force;

      // calculate the segment layout
      var segs = ["JavaStack", "ReactStack","Configuration", "App","Database","Monitoring","Config","Security","Deploy","Runtime","Infrastructure","Develop","Registry"]
      for ( var p = 0; p < segs.length; p++) {
          var tot = 0;
          var x,num;
          for ( x = 0 ; x < graph.nodes.length; x++) {
              if (graph.nodes[x].title.segment == segs[p]) {
                  tot++;
              }
          }
          for ( num = 0,x = 0 ; x < graph.nodes.length; x++) {
              if (graph.nodes[x].title.segment == segs[p]) {
                  graph.nodes[x].layout = { "seq":num,"total":tot,"seg":segs[p] };
                  num++;
              }
          }
      }

      const links = vis.select("g.all_lines").selectAll("path")
          .data(graph.links);

      links.enter().insert("path")
          .attr("class", "link");
      // .attr("marker-end", function(d) { return "url(#" + 'suit' + ")"; });

      links.exit().remove();

      const nodeSet = vis.select("g.nodeSet").selectAll("g.node")
          .data(graph.nodes);

      const newNodes = nodeSet.enter().append("g")
          .attr("class", "node")
          .call(force.drag);

      new HexShape().build(newNodes, "hex");
      new CircleShape().build(newNodes, "circle");
      new MiniCircleShape().build(newNodes, "minicircle");
      new RectangleShape().build(newNodes, "rectangle");
      new BuildingBlockShape().build(newNodes, "buildingBlock");
      new BuildingBlockMicroShape().build(newNodes, "buildingBlockMicro");
      new BuildingBlockMicroShape().build(newNodes, "stackSolutionMicro");
      new BuildingBlockShape().build(newNodes, "stackSolution");
      new CommentShape().build(newNodes, "comment");
      new IconShape().build(newNodes, "icon");
      new ZoneShape().build(newNodes, "zone");
      new ImageShape().build(newNodes, "image");
      new LifecycleShape().build(newNodes, "lifecycle");
      new TableShape().build(newNodes, "table");
      new MultiItemShape().build(newNodes, "multiItemSection");
      new ShapedItem().build(newNodes, "shapedItem");
      new ShapedItemVertical().build(newNodes, "shapedItemVertical");
      new StackedBuildingBlockShape().build(newNodes, "stackedBuildingBlock");
      new StackedBuildingBlockShape().build(newNodes, "stackedBuildingBlockMicro");

      // Use x and y if it was provided, otherwise stick it in the center of the zone
      newNodes
          .attr("cx", function(d) { const dim = self.getDim(d); return d.x = (d.x ? d.x : (dim.x1 + (dim.x2-dim.x1)/2)); })
          .attr("cy", function(d) { const dim = self.getDim(d); return d.y = (d.y ? d.y : (dim.y1 + (dim.y2-dim.y1)/2)); });

      newNodes
        .on('mouseenter', function (nodeSet, index) {
            $(this).addClass("node-hover");
        })
        .on('mouseleave', function (e) {
            $(this).removeClass("node-hover");
        })
        .on('mousedown', function (e) {
            self.downX = Math.round(e.x + e.y);
        })
        .on('mouseup', function (e) {
            let nodes = [];
            $(graph.nodes).each(function (i) {
              const n = graph.nodes[i];
              console.log("S : " +n.name);
              nodes.push({"name":n.name, "title":n.title, "x":Math.round(n.x), "y":Math.round(n.y), "type":n.type, "fixed":(n.fixed ? true:false), "boundary":n.boundary});
            })
            console.log("OUT: " +JSON.stringify(nodes, null, 2));
        })
        .on('click', function (e) {
            if (self.downX == Math.round(e.x + e.y)) {
              self.navigation.toggle(e);
            } else {
              self.navigation.close(e);
            }
        })

      nodeSet.exit().remove();

      this.navigation.update();

      force.on("tick", function() {
          const radius = 20;
          const w = vis.attr("width");
          const h = vis.attr("height");


          // enforce zone boundaries
  //          nodeSet
  //              .attr("cx", function(d) { const dim = self.getDim(d); d.x = Math.max(dim.x1 + radius, Math.min(dim.x2 - radius, d.x)); return 0;})
  //              .attr("cy", function(d) { const dim = self.getDim(d); d.y = Math.max(dim.y1 + radius, Math.min(dim.y2 - radius, d.y)); return 0;});

          nodeSet
              //.each(self.collide(0.5))
              .attr("transform", function(d) {
                  return "translate(" + d.x + "," + d.y + ")";
              });

          var ln = d3.svg.line()
              .x(function (d) { return d.x; })
              .y(function (d) { return d.y; })
              .interpolate("linear"); // step, linear

          var stepLine = d3.svg.line()
              .x(function (d) { return d.x; })
              .y(function (d) { return d.y; })
              .interpolate("step-before"); // step, step-after, step

          links
            .attr("d", function (d) {
              const dx = Math.abs((d.target.x - d.source.x)/2);
              const dy = Math.abs((d.target.y - d.source.y)/2);
              //{"x":d.source.x + dx,"y":d.source.y + dy}
              if (d.type == "straight") {
                const lineData = [{"x":d.source.x, "y":d.source.y},  {"x":d.target.x, "y":d.target.y}];
                return stepLine(lineData);
              } else {
                const lineData = [{"x":d.source.x, "y":d.source.y}, {"x":d.target.x, "y":d.target.y}];
                return ln(lineData);
              }
          });


      });

      // Restart the force layout
      force
        .nodes(graph.nodes)
        .links(graph.links)
        .start();
  }


  // Resolves collisions between d and all other nodes.
  collide(alpha) {
    let graph = this.state.graph;

    const padding = 1.5,
          maxRadius = 12;
    const quadtree = d3.geom.quadtree(graph.nodes);

    return function(d) {
      d.radius = 25;
      let r = d.radius + maxRadius + padding,
          nx1 = d.x - r,
          nx2 = d.x + r,
          ny1 = d.y - r,
          ny2 = d.y + r;

      if (d.x == null) {
          console.log("Ignoring.. " + d.name);
          return;
      }
      quadtree.visit(function(quad, x1, y1, x2, y2) {
        //console.log("Match: " + x1+","+y1+","+x2+","+y2 + " : " + quad.point);
        //if (quad.leaf == false) {
        //    return true;
        //}
        // quad.point has the object details
        if (quad.point && (quad.point !== d)) {

          if (quad.point.boundary != d.boundary) {
              //return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
          }
          //if (d.x == null) {
          //  return true;
          //}
          //console.log("D = "+JSON.stringify(d, null, 2));
          //console.log("Quad = "+JSON.stringify(quad, null, 2));
          quad.point.radius = 25;
          let x = d.x - quad.point.x,
              y = d.y - quad.point.y,
              l = Math.abs(Math.sqrt(x * x + y * y)),
              r = d.radius + quad.point.radius + padding;
          //console.log("X="+x+",Y="+y);
          //console.log("L="+l+",R="+r);
          if (l < r) {
            l = (l - r) / l * alpha;

            // Need to make sure we are not moving it outside of boundary
            x = x * l;
            y = y * l;
            if (isNaN(x) || isNaN(y)) {
              console.log("ILLEGAL VALUE!");
              x = 0.00;
              y = 0.01;
              //return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
            }
            //console.log("Changing..." + x+", "+y+", dx ="+d.x+", "+d.y);
            d.x = d.x - x;
            d.y = d.y - y;
            //console.log("ANSWER: d="+d.x+", "+d.y);
            quad.point.x += x;
            quad.point.y += y;
            //console.log("Change to: " +d.x+", "+d.y+" :: " + quad.point.x+", "+quad.point.y);
          }
        }
        //console.log("Answer: " +x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1);
        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
      });
    };
  }

  addNode(name, fixed, type, boundary) {
      if (boundary == "A" || boundary == "D" ) fixed = true;
      const nd = {"name":name, "fixed":fixed, "type":type, "boundary":boundary };
      this.graph.nodes.push(nd);
      this.update();
  }

  removeNode(name) {
      const graph = this.state.graph;
      graph.nodes = graph.nodes.filter(function(node) {return (node["name"] != name)});
      graph.links = graph.links.filter(function(link) {return ((link["source"]["name"] != name)&&(link["target"]["name"] != name))});
      this.update();
  }

  findNode(name) {
      const graph = this.state.graph;
      for (let i in graph.nodes) if (graph.nodes[i]["name"] === name) return graph.nodes[i];
  }

  registerLinks (links) {
      for (var i = 0 ; i < links.length; i++) {
          this.addLink(links[i].source, links[i].target, links[i].type);
      }
  }

  addLink(source, target, type) {
      const graph = this.state.graph;
      graph.links.push({"type":type, "source":this.findNode(source),"target":this.findNode(target)});
  }

  render() {
    console.log("Render ViewerCanvas");
    if (this.state) {
      this.update();
      this.registerLinks(this.state.links);
      this.update();
    }
    return (
      <svg />
    );
  }
}

ViewerCanvas.propTypes = {
  graph: PropTypes.object.isRequired,
  eid: PropTypes.string.isRequired
};

export default ViewerCanvas;

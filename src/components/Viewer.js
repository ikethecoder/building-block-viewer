import React, {PropTypes} from 'react';

import $ from 'jquery';

import ReactDOM from 'react-dom';

import ViewerBox from './ViewerBox.js';
import ViewerCanvas from './ViewerCanvas.js';

class Viewer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.save = this.save.bind(this);
    this.onTimeframeChange = this.onTimeframeChange.bind(this);
    this.fuelSavingsKeypress = this.fuelSavingsKeypress.bind(this);
  }

  fuelSavingsKeypress(name, value) {
    this.props.calculateFuelSavings(this.props.appState, name, value);
  }

  save() {
    //this.props.saveFuelSavings(this.props.appState);
      const obj = ReactDOM.findDOMNode(this);

      const width = obj.offsetWidth;
      console.log("Width: " + width);

  }

  componentDidMount() {
      const obj = ReactDOM.findDOMNode(this);

      console.log("id = "+obj.id);
      const width = obj.offsetWidth;
      console.log("Width: " + width);

      //this.loadDataFromServer();


      const g = {
        "nodes": [
            {"name": "Digital", "type":"hex", "boundary": "A"},
            {"name": "IDE", "type":"hex", "boundary": "F"},
            {"name": "UTE", "type":"hex", "boundary": "F"},
            {"name": "Perf", "type":"hex", "boundary": "F"},
            {"name": "Live", "type":"hex", "boundary": "F"},
            {"name": "CD_Env", "type":"hex", "boundary": "F"},

            {"name": "CD", "type":"circle", "boundary": "C"},
            {"name": "Go.CD", "type":"hex", "boundary": "B"},
            {"name": "Go.CD Agent", "type":"hex", "boundary": "B"},
            {"name": "Ansible", "type":"hex", "boundary": "B"},
            {"name": "etcd", "type":"hex", "boundary": "D", "fixed": true},
            {"name": "GitLab", "type":"hex", "boundary": "D"},
            {"name": "Rocket.Chat", "type":"hex", "boundary": "E"},

            {"name": "App-1", "type":"circle", "boundary": "C"},
            {"name": "App-2", "type":"circle", "boundary": "C"},
            {"name": "App-3", "type":"circle", "boundary": "C"},

            {"name": "MongoDB", "type":"hex", "boundary": "D"},
            {"name": "Go.CD Agent_2", "type":"hex", "boundary": "B"},
            {"name": "etcd_2", "type":"hex", "boundary": "D"},
            {"name": "Kong", "type":"hex", "boundary": "B"},
            {"name": "NGINX", "type":"hex", "boundary": "B"},

            {"name": "Source Control", "type":"hex", "boundary": "B"},
            {"name": "do-sf-1", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-2", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-4", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-5", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-6", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-7", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-8", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-9", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-10", "type":"minicircle", "boundary": "D", "fixed": false},
            {"name": "do-sf-3", "type":"minicircle", "boundary": "D", "fixed": false}
        ],
        "links": [
        ]
      }

      const links = [
//        {"source":"Digital", "target":"IDE", "type":""},
//        {"source":"Digital", "target":"UTE", "type":""},
//        {"source":"Digital", "target":"Perf", "type":""},
//        {"source":"Digital", "target":"Live", "type":""},
        {"source":"App-1", "target":"MongoDB", "type":""},
        {"source":"App-1", "target":"Go.CD Agent_2", "type":""},
        {"source":"App-1", "target":"etcd_2", "type":""},
        {"source":"App-1", "target":"Kong", "type":""},
        {"source":"App-1", "target":"NGINX", "type":""},
        {"source":"CD", "target":"GitLab", "type":""},
        {"source":"CD", "target":"etcd", "type":""},
        {"source":"CD", "target":"Go.CD", "type":""},
        {"source":"CD", "target":"Ansible", "type":""},
        {"source":"CD", "target":"Go.CD Agent", "type":""},
        {"source":"etcd", "target":"do-sf-1", "type":""},
        {"source":"etcd", "target":"do-sf-2", "type":""},
        {"source":"etcd", "target":"do-sf-4", "type":""},
        {"source":"etcd", "target":"do-sf-5", "type":""},
        {"source":"etcd", "target":"do-sf-6", "type":""},
        {"source":"etcd", "target":"do-sf-7", "type":""},
        {"source":"etcd", "target":"do-sf-8", "type":""},
        {"source":"etcd", "target":"do-sf-9", "type":""},
        {"source":"etcd", "target":"do-sf-10", "type":""},
        {"source":"etcd", "target":"do-sf-3", "type":""}
      ];

      this.refs.canvas.setState({graph: g, links: links});
      this.setState({graph: g, links: links});

      const self = this;
      $(window).resize(function (e) {
          self.refs.canvas.refreshSize();
      });

  }

  onTimeframeChange(e) {
    //this.props.calculateFuelSavings(this.props.appState, 'milesDrivenTimeframe', e.target.value);
  }


  loadDataFromServer() {
      let self = this;

      const eid = '572f9954d4c67f8e5573d7dd';

      console.log("loading data from server...");
      const url = "http://localhost:8888/api/saasexpress/platform/" + eid;
      let request = $.get(url, function (response) {

          let links = response.links;
          response.links = [];

          //self.graph = response;
          //self.refs.canvas.setGraph(response);
          //self.setState(response);
          //self.recalcSpace();
          self.refs.canvas.setState({"graph":response, "links":links});
          //self.refs.canvas.update();
//
//          if (links) {
//              for (var i = 0 ; i < links.length; i++) {
//                  self.refs.canvas.addLink(links[i].source, links[i].target, links[i].type);
//              }
//
//          }
      });
  }

//      <div style={{width:"9933px",height:"7017px"}} className="canvas">

  render() {

    let w = 700;
    let h = 800;
    let margin = 10;


    const zones = [
        { "type" : "swimlane", "rectangle": [margin, margin, w/4 - margin - 5, margin + 100 - 10], "title": "Platform" },
        { "type" : "swimlane", "rectangle": [w/4 + 5, margin, w/4 - 10, h - (2*margin) - 10], "title": "Technologies" },
        { "type" : "swimlane", "rectangle": [w/2 + 5, margin, w/4 - margin - 10, h - (2*margin) - 10], "title": "Plan" },
        { "type" : "swimlane", "rectangle": [3*w/4 - margin + 5, margin, w/4 - 10, h - (2*margin) - 10], "title": "Resources" },
        { "type" : "swimlane", "rectangle": [margin, margin + 100 + 5, w/4 - margin - 5, margin + 100], "title": "Collaboration" },
        { "type" : "swimlane", "rectangle": [margin, margin + 240 + 10, w/4 - margin - 5, h - margin - 300], "title": "Environments" },
        { "type" : "swimlane", "rectangle": [w - margin + 10, margin, 80, h - (2*margin) - 10], "title": "Billable Nodes" }
    ];

    return (
      <div style={{width:"100%"}} className="canvas">
        <ViewerCanvas ref="canvas" zones={zones} graph="{this.graph}"/>
      </div>
    );
  }
          /*
          <pre style={{display:"none"}}>{JSON.stringify(this.state, null, 2)}</pre>

          <div style={{padding:"10px"}}>
            <img src="images/canzea_logo.svg" style={{width:"400px"}}/>
          </div>

          <input type="submit" value="Save" onClick={this.save}/>
           */
}


Viewer.propTypes = {
};

export default Viewer;

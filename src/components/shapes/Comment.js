
import d3 from 'd3';
import React, {PropTypes} from 'react';

class CircleShape {

  build(newNodes, domainType) {

      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      chgSet.append("rect")
          .attr("x", 0)
          .attr("y", -10)
          .attr("width", 100)
          .attr("height", 15);

      chgSet.append("text")
          .attr("class", "labelText")
          .attr("text-anchor", "left")
          .attr("dx", 0)
          .attr("dy", 0)
          .text(function(d) { return d.title.label; });
  }
}

export default CircleShape;


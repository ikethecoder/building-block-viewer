
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

class MultiItemShape {

  build(newNodes, domainType) {
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      var x;
      var y;
      var A;

      var obj = chgSet;//.append("g");
      //obj.attr("transform", "translate(400, 260)")

       var tiles = [
            {
              "name": "REA01",
              "title": {
                "solution": "React"
              },
              "x": 85,
              "y": 270,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA02",
              "title": {
                "solution": "Redux"
              },
              "x": 85,
              "y": 288,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA03",
              "title": {
                "solution": "Babel"
              },
              "x": 127,
              "y": 323,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA04",
              "title": {
                "solution": "Webpack"
              },
              "x": 85,
              "y": 305,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA05",
              "title": {
                "solution": "Browsersync"
              },
              "x": 43,
              "y": 287,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA06",
              "title": {
                "solution": "Mocha"
              },
              "x": 85,
              "y": 323,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA07",
              "title": {
                "solution": "Isparta"
              },
              "x": 44,
              "y": 305,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA08",
              "title": {
                "solution": "TrackJS"
              },
              "x": 127,
              "y": 305,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA09",
              "title": {
                "solution": "ESLint"
              },
              "x": 127,
              "y": 270,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA10",
              "title": {
                "solution": "SASS"
              },
              "x": 126,
              "y": 287,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA11",
              "title": {
                "solution": "npm"
              },
              "x": 43,
              "y": 270,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            }
      ]
      buildMatrix(4, 160, 225, 90, false, {x:-80,y:0}, 82, 0, tiles);

      tiles = [
  {
    "name": "JAV01",
    "title": {
      "solution": "java"
    },
    "x": 70,
    "y": 208,
    "type": "stackSolutionMicro",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV02",
    "title": {
      "solution": "spring"
    },
    "x": 124,
    "y": 225,
    "type": "stackSolutionMicro",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV03",
    "title": {
      "solution": "selenium"
    },
    "x": 124,
    "y": 207,
    "type": "stackSolutionMicro",
    "fixed": true,
    "boundary": "A"
  }
      ]
      buildMatrix(4, 160, 225, 90, false, {x:-80,y:0}, 82, 1, tiles);

      tiles = [
  {
    "name": "JAV01",
    "title": {
      "solution": "java"
    },
    "x": 70,
    "y": 208,
    "type": "stackSolutionMicro",
    "fixed": true,
    "boundary": "A"
  }

      ]
      buildMatrix(4, 160, 225, 90, false, {x:-80,y:0}, 82, 2, tiles);
      buildMatrix(4, 160, 225, 90, false, {x:-80,y:0}, 82, 3, tiles);


      function buildMatrix (segments, radius, base, range, side, delta, inner=82, s, tiles) {
          var extended = (side ? "350":"-370");

          // fill the space
          // Find whether it is divisible by 5,3,2,1 and fill the space accordingly


          var tileCount = tiles.length;
          var iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
          if (iMax == tileCount) {
              tileCount++;
              iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
              tileCount--;
          }
          var dvMax = Math.ceil(tileCount / iMax)
          var tile = tiles.length - 1;

          var width = 145/dvMax;//Math.round((baseX - (extended - delta.x)) / iMax);

          for (var i = 0; i < iMax; i++) {

              for (var dv = 0; dv < dvMax; dv++) {
                  // We need to calculate two points
                  // One at the base angle and the other at the other angle

                  var A = (base - 90 + ((s+((i+1)*(1/iMax))) * range/segments)) * (Math.PI/180);
                  var x = (radius) * Math.cos(A) - 1;
                  var y = (radius) * Math.sin(A);

                  var A1 = (base - 90 + ((s+(i*(1/iMax))) * range/segments)) * (Math.PI/180);
                  var x1 = (radius) * Math.cos(A1) - 1;
                  var y1 = (radius) * Math.sin(A1);

                  var cx = radius;
                  var cy = radius;

                  if (dv == 0) {
                      obj.append("path")
                        .attr("d", "M" + x+","+y+" A" + cx + "," + cy + " 0 0,0 " + (x1) + "," + y1 +" L" + (x1-width) + "," + (y1) + " A" + cx + "," + cy + " 0 0,1 " + (x-width) + "," + (y) + " L" + (x) + "," + (y))
                        .attr("stroke-width", "0.05em")
                        .attr("stroke", "#8cb7e2")
                        .attr("fill", "#white")
                        .attr("transform", "translate("+delta.x+","+delta.y+")")
                  } else {
                      var shift = (dv * (width+(x1-x) + 5));
                      x = x - (dv*(width+0))
                      x1 = x1 - (dv*(width+0));
                      obj.append("path")
                        .attr("d", "M" + x+","+y+" A" + cx + "," + cy + " 0 0,0 " + (x1) + "," + y1 +" L" + (x1-width) + "," + (y1) + " A" + cx + "," + cy + " 0 0,1 " + (x-width) + "," + (y) + " L" + (x) + "," + (y))
                        .attr("stroke-width", "0.05em")
                        .attr("stroke", "#8cb7e2")
                        .attr("fill", "#ffffff")
                        .attr("transform", "translate("+delta.x+","+delta.y+")")
                  }
                  var _name = tiles[tile].title.solution;//tiles[(dv*iMax) + i]['name'];
                  obj.append("text")
                      .attr("class", "title")
                      .attr("text-anchor", "middle")
                      .attr("dominant-baseline", "central")
                      .attr("dx", 0)
                      .attr("dy", 0)
                      .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + (((x+x1)/2)-(width/2))+","+(y+((y1-y)/2))+") rotate(" + 0 + ")")
                      .text(function(d) { return _name; });

                  tile--;
                  if (tile < 0) {
                    break;
                  }
              }
              if (tile < 0) {
                break;
              }
          }
      }
  }
}

export default MultiItemShape;


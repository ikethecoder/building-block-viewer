
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

import dutil from './2dUtils'

class SectionLeft {

  build(vis, domainType) {
      var twodUtils = new dutil()

      let obj = vis.append("g");

      obj.attr("transform", "translate(402, 266)")
      obj.classed("_" + domainType, true);

      // App Building Blocks
      twodUtils.buildBottomVerticalBase(obj);
      //twodUtils.buildLevel1(obj, 135, 90, false, "", {x:0,y:0});


      //buildLevel2Labels(4, 90, 180, ["Network","Security","Data", "Monitoring", ""], 160, {x:0,y:100});
      //twodUtils.buildLevel4Vertical(obj, 4, 125, 90, 180, false, ["Monitoring","Data","Security","Service Mgmt"], {x:0,y:0});

  }
}

export default SectionLeft;


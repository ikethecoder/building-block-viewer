
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

class twodUtilsClass {

  focusAt (x1,y1,x2,y2,x3,y3,x4,y4) {
      var coords = []
      var lowPoint = {x:x3, y:y3 }
      coords.push(x1 - lowPoint.x);
      coords.push(y1 - lowPoint.y);
      coords.push(x2 - lowPoint.x);
      coords.push(y2 - lowPoint.y);
      coords.push(x3 - lowPoint.x);
      coords.push(y3 - lowPoint.y);
      coords.push(x4 - lowPoint.x);
      coords.push(y4 - lowPoint.y);
      coords.push(lowPoint.x)
      coords.push(lowPoint.y)
      return coords;
  }

  calculateCoordinate (radius, angle) {
      console.log("2dUtils.CalculateCoordinates : " + radius + " : " + angle);
      var A = (angle - 90) * (Math.PI/180);
      var x = (radius) * Math.cos(A) ;
      var y = (radius) * Math.sin(A);
      return {x:x, y:y}
  }

  calculateX (radius, y, deg) {
      console.log("CalculateCoordinates : " + radius + " : " + deg);
      var A = (deg) * (Math.PI/180);
      //var x = (radius) * Math.cos(A) ; // radius = x/Math.cos(A) = y/Math.sin(A) -> y = x*Math.sin(A)/Math.cos(A)
      var x = y * Math.cos(A) / Math.sin(A);
      return {x:x, y:y}
  }

  calculateTilePosition (tileNumber, tileCount, segment) {
      var iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
      if (iMax == tileCount) {
          tileCount++;
          iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
          tileCount--;
      }
      var dvMax = Math.ceil(tileCount / iMax)
      var tile = tileCount - 1;

      var width = 145/dvMax;//Math.round((baseX - (extended - delta.x)) / iMax);

      iMax = tileCount;
      dvMax = 1;
      for (var i = 0; i < iMax; i++) {

          for (var dv = 0; dv < dvMax; dv++) {
              if (tile == tileNumber) {
                  return {row:i, col:dv, width:145/dvMax, maxRow:iMax, maxCol:dvMax, seg:segment};
              }
              tile--;
          }
      }
      alert("Tile not found!");
  }




  buildLevel1(obj, base, size, side, label, delta) {
      var arc = d3.svg.arc()
          .innerRadius(25)
          .outerRadius(80)
          .startAngle(base * (Math.PI/180)) //converting from degs to radians
          .endAngle((base + size) * (Math.PI/180)) //just radians

      obj.append("path")
      .attr("d", arc)
      .attr("stroke", "white")
      .attr("stroke-width", "0.1em")
      .attr("fill", "lightblue")
      .attr("transform", "translate("+delta.x+","+delta.y+")")


      let anchor = "left";
      let rotation = (base - 90 + (size/2));
      //if (base > 180 ) { rotation = rotation - 360; }
      var radius = 30;
      if (side == false) { rotation = 0; radius = 0; anchor = "middle";}
      var A = rotation * (Math.PI/180);
      var x = (radius) * Math.cos(A);
      var y = (radius) * Math.sin(A);

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", anchor)
          .attr("dx", 0)
          .attr("dy", 0)
          .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
          .text(function(d) { return label; });


  }


  buildLevel1Solutions(obj, base, size, side, label, delta) {
      var arc = d3.svg.arc()
          .innerRadius(25)
          .outerRadius(35)
          .startAngle(base * (Math.PI/180)) //converting from degs to radians
          .endAngle((base + size) * (Math.PI/180)) //just radians

      obj.append("path")
      .attr("d", arc)
      .attr("stroke", "white")
      .attr("stroke-width", "0.1em")
      .attr("fill", "#CCCCCC")
      .attr("transform", "translate("+delta.x+","+delta.y+")")


      let anchor = "left";
      let rotation = (base - 90 + (size/2));
      //if (base > 180 ) { rotation = rotation - 360; }
      var radius = 30;
      if (side == false) { rotation = 0; radius = 0; anchor = "middle";}
      var A = rotation * (Math.PI/180);
      var x = (radius) * Math.cos(A);
      var y = (radius) * Math.sin(A);

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", anchor)
          .attr("dx", 0)
          .attr("dy", 0)
          .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
          .text(function(d) { return label; });


  }


  buildLevel2(obj, segments, base, range, _names, outer, delta, inner=82) {


    for ( var x = 0; x < segments; x++) {

      var start = base + (x * range / segments);
      var end = base + ((x+1) * range / segments) - 0.0;
      var arc3 = d3.svg.arc()
          .innerRadius(inner)
          .outerRadius(outer)
          .startAngle(start * (Math.PI/180)) //converting from degs to radians
          .endAngle(end * (Math.PI/180)) //just radians

      obj.append("path")
        .attr("d", arc3)
        .attr("stroke-width", "0.0em")
        .attr("stroke", "white")
        .attr("transform", "translate(" + delta.x+","+delta.y+")")
        .attr("fill", (base == 315 || base == 135 ? "#e2cc93":"#9fcefe"))

    }

    var radius = 90;
    for ( var s = 0; s < segments; s++) {
        let mid = (range/segments) / 2;
        let rotation = (base - 90 + mid + (s * range/segments));// * (Math.PI/180);

        var A = rotation * (Math.PI/180);

        if (rotation > 130 && rotation <= 220) {
          rotation = rotation - 180;
          //A = (rotation+180) * (Math.PI/180);
          x = (radius + 50) * Math.cos(A);
          var y = (radius + 50) * Math.sin(A);
          const _name = _names[s];
          obj.append("text")
              .attr("class", "title")
              .attr("text-anchor", "left")
              .attr("dx", 0)
              .attr("dy", 0)
              .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
              //.attr("transform", "translate(" + delta.x+","+delta.y+")")
              .text(function(d) { return _name; });
        } else if ((rotation > 45 && rotation <= 130) || (rotation < -45 || rotation > 220)) {
          rotation = 0;
          //A = (rotation+180) * (Math.PI/180);
          x = (radius + 50) * Math.cos(A);
          y = (radius + 50) * Math.sin(A);
          const _name = _names[s];
          obj.append("text")
              .attr("class", "title")
              .attr("text-anchor", "middle")
              .attr("dx", 0)
              .attr("dy", 0)
              .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
              //.attr("transform", "translate(" + delta.x+","+delta.y+")")
              .text(function(d) { return _name; });
        } else {
          x = (radius + 20) * Math.cos(A);
          y = (radius + 20) * Math.sin(A);
          const _name = _names[s];
          obj.append("text")
              .attr("class", "title")
              .attr("text-anchor", "left")
              .attr("dx", 0)
              .attr("dy", 0)
              .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
              .text(function(d) { return _name; });
        }

            ///translate(" + x+","+y+")

    }
  }

  buildLevel2Labels(obj, segments, base, range, _names, outer, delta, inner=82) {


    var radius = 100;
    for ( var s = 0; s < segments; s++) {
        let mid = (range/segments) / 2;
        let rotation = (base - 90 + mid + (s * range/segments));// * (Math.PI/180);

        var A = rotation * (Math.PI/180);
        var x = (radius + 20) * Math.cos(A);
        var y = (radius + 20) * Math.sin(A);
        const _name = _names[s];
        obj.append("text")
            .attr("class", "title")
            .attr("text-anchor", "left")
            .attr("dx", 0)
            .attr("dy", 0)
            .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + 0 + ")")
            .text(function(d) { return _name + rotation; });

            ///translate(" + x+","+y+")

    }
  }


  buildLevel4Vertical(obj, segments, radius, base, range, side, _names, delta) {

      var self = this;
      var extended = (side ? -250:250);
      for ( var s = 1; s < segments; s++) {

          var start = base + (s * range / segments);
          var A = (base - 90 + (s * range/segments)) * (Math.PI/180);
          var x = (radius + 20) * Math.cos(A);
          var y = (radius + 20) * Math.sin(A);

          var A0 = (45 - 90 + (s * range/segments)) * (Math.PI/180);
          var x0 = 82 * Math.cos(A);
          var y0 = 82 * Math.sin(A);

          obj.append("path")
              .attr("d", "M" + x0 + ","+ y0 + "L" + (x) + ","+(y)+" L" + x+"," + extended + "")
              .attr("stroke-width", "0.07em")
              .attr("stroke", "white")
              .attr("fill", "none")
              .attr("transform", "translate("+delta.x+","+delta.y+")")
      }

      for ( s = 0; s < segments; s++) {
          start = base + (s * range / segments);
          A = (base - 90 + (s * range/segments)) * (Math.PI/180);
          x = (radius + 20) * Math.cos(A);
          y = (radius + 20) * Math.sin(A);
          y = radius + 20;
          x = x - 20;
          const _name = _names[s];
          obj.append("text")
              .attr("class", "title")
              .attr("text-anchor", "middle")
              .attr("dx", 0)
              .attr("dy", 0)
              .attr("transform", function(d) {
                var c1 = self.calculateCoordinate(120, 130 + (s*20));
                return "translate(" + delta.x+","+delta.y+") translate(" + c1.x + ","+ c1.y +") rotate(" + 0 + ")"
              })
              .text(function(d) { return _name; });

      }
  }





  buildLevel4Lines(obj, segments, radius, base, range, side, delta, inner=82) {

    var extended = (side ? "300":"-188");

    radius = radius - 20;
    for ( var s = 0; s < segments; s++) {

        var start = base + (s * range / segments);
        var end = base + ((x+1) * range / segments);
        var A = (base - 90 + (s * range/segments)) * (Math.PI/180);
        var x = (radius + 20) * Math.cos(A);
        var y = (radius + 20) * Math.sin(A);

        var A0 = (45 - 90 + (s * range/segments)) * (Math.PI/180);
        var x0 = inner * Math.cos(A);
        var y0 = inner * Math.sin(A);

        obj.append("path")
            .attr("d", "M" + x0 + ","+ y0 + "L" + (x) + ","+(y)+" L" + extended+"," + y + "")
            .attr("stroke-width", "0.07em")
            .attr("stroke", "white")
            .attr("fill", "none")
            .attr("transform", "translate("+delta.x+","+delta.y+")")

    }
  }


  buildLevel5(obj, segments, radius, base, range, side, delta, inner=82) {

    var extended = (side ? "380":"-188");

    for ( var s = 0; s < segments; s++) {

  //    var start = base + (s * range / segments);
  //    var end = base + ((s+1) * range / segments) - 0.5;
  //    var arc3 = d3.svg.arc()
  //        .innerRadius(182)
  //        .outerRadius(210)
  //        .startAngle(start * (Math.PI/180)) //converting from degs to radians
  //        .endAngle(end * (Math.PI/180)) //just radians
  //
  //    obj.append("path")
  //      .attr("d", arc3)
  //      .attr("fill", "#8cb7e2")

      var A = (base - 90 + (s * range/segments)) * (Math.PI/180);
      var x = (radius) * Math.cos(A);
      var y = (radius) * Math.sin(A);

      var A1 = (base - 90 + ((s+1) * range/segments)) * (Math.PI/180);
      var x1 = (radius) * Math.cos(A1);
      var y1 = (radius) * Math.sin(A1);
      var height = y1-y; //y1-y;//Math.sqrt(Math.pow(s * range/segments, 2)/2);

      //A160,160 0 0,1 -72.63847995832751,-142.56104387013886
      var cx = radius;
      var cy = radius;
      var wid = extended - delta.x;
      obj.append("path")
        .attr("d", "M" + x+","+y+" A" + cx + "," + cy + " 0 0,1 " + (x1) + "," + (y+height) + " L" + wid + "," + (y+height) + " L" + wid+","+y)
        .attr("stroke-width", "0.0em")
        .attr("stroke", "white")
        .attr("fill", "#8cb7e2")
        .attr("transform", "translate("+delta.x+","+delta.y+")")

      }

  }

  buildTopVerticalBase(obj) {

        var x = 15;
        var y = 0;
        var radius = 230;
        var slant = 115;
        var height = 115;
        var x1 = 270;
        var y1 = 0;
        var cx = radius;
        var cy = radius;
        var delta = {x:-145,y:-170};
        obj.append("path")
          .attr("d", "M" + (x-slant)+","+y+" A" + 0 + "," + 0 + " 0 0,1 " + (x1 + slant) + "," + y + " L" + x1 + "," + (height) + " A" + cx + "," + cy + " 0 0,0 " + (x) + "," + height +" L" + (x-slant) + "," + y + ")")
          .attr("stroke-width", "0.0em")
          .attr("stroke", "white")
          .attr("fill", "#e2cc93")
          .attr("transform", "translate("+delta.x+","+delta.y+")")
          // lightblue #e2cc93
  }

  buildBottomVerticalBase(obj) {

        var x = 12;
        var y = -15;
        var radius = 235;
        var slant = 108;
        var height = 100;
        var x1 = 282;
        var y1 = -15;
        var cx = 800;
        var cy = 800;
        var delta = {x:-145,y:70};
        obj.append("path")
          .attr("d", "M" + (x)+","+y+" A" + radius + "," + radius + " 0 0,0 " + (x1) + "," + y + " L" + (x1+slant) + "," + (height) + " L" + (x1+slant) + "," + (height+45) + " A" + cx + "," + cy + " 0 0,1 " + (x-slant) + "," + (height+45) +" L" + (x-slant) + "," + (height) + " L" + (x) + "," + y + ")")
          .attr("stroke-width", "0.0em")
          .attr("stroke", "white")
          .attr("fill", "#e2cc93")
          .attr("transform", "translate("+delta.x+","+delta.y+")")
  }


  buildTopComputeRequirement(obj) {

      var x = 0;
      var y = 0;
      var radius = 0;
      var slant = 0;
      var height = 15;
      var x1 = 550;
      var y1 = 0;
      var cx = radius;
      var cy = radius;
      var delta = {x:-280,y:-235};
      obj.append("path")
        .attr("d", "M" + (x)+","+y+" L" + (x1) + "," + y + " L" + x1 + "," + (height) + " L" + (x) + "," + height +" L" + (x) + "," + y + ")")
        .attr("stroke-width", "0.0em")
        .attr("stroke", "white")
        .attr("fill", "#e2cc93")
        .attr("transform", "translate("+delta.x+","+delta.y+")")

      x = 560;
      x1 = 660;
      obj.append("path")
        .attr("d", "M" + (x)+","+y+" L" + (x1) + "," + y + " L" + x1 + "," + (height) + " L" + (x) + "," + height +" L" + (x) + "," + y + ")")
        .attr("stroke-width", "0.0em")
        .attr("stroke", "white")
        .attr("fill", "#e2cc93")
        .attr("transform", "translate("+delta.x+","+delta.y+")")

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", -226)
          .text(function(d) { return "Environmental Compute Specifications"; });

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 330)
          .attr("dy", -226)
          .text(function(d) { return "Network"; });
  }

}

export default twodUtilsClass;


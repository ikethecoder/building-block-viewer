
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

class TableShape {

  build(newNodes, domainType) {
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      function displayData (chgSet, data) {
        const products = {
          "I1": {"cores":"1 CPU", "memory":"1 GB"},
          "I2": {"cores":"2 CPU", "memory":"2 GB"},
          "I4": {"cores":"2 CPU", "memory":"4 GB"},
          "I8": {"cores":"4 CPU", "memory":"8 GB"},
          "I16": {"cores":"8 CPU", "memory":"16 GB"}
        }
        $(data.title.order).each(function (r, row) {
          const cols = [ "zone", "qty", "cores", "memory" ];
          $(cols).each(function (d, text) {
            chgSet.append("text").filter(function (d, i) { return d.title.label === data.title.label;})
                .attr("class", "cell")
                .attr("text-anchor", "middle")
                .attr("dx", 10 + (25 * d))
                .attr("dy", 0 + (r * 9))
                .text(function(d) { if (text == "qty") { return row.qty; } else if (text == "zone") { return "A"; } else { return products[row.product][text]; } });
          });
        });
      }


      chgSet.filter(function(d) { return d.title.pipeline == false; }).append("rect")
          .attr("x", 0)
          .attr("y", -22)
          .attr("width", 100)
          .attr("height", 40)
          .attr("stroke-width", "0.01em")
          .attr("stroke", "#333")
          .attr("fill", "white")

      chgSet.filter(function(d) { return d.title.pipeline == true; }).append("path")
          .attr("d", "M0,-22 L-10,-22 L0,-2 L-10,18 L90,18 L100,-2 L90,-22 L0,-22")
          .attr("stroke-width", "0.01em")
          .attr("stroke", "#333")
          .attr("fill", "white")

      chgSet.append("text")
          .attr("class", "title")
          .attr("text-anchor", "left")
          .attr("dx", 2)
          .attr("dy", -14)
          .text(function(d) {
            displayData (chgSet, d)
            return d.title.label.toUpperCase(); });

      chgSet.append("line")
          .attr("x1", 1)
          .attr("y1", -6)
          .attr("x2", 96)
          .attr("y2", -6)
          .attr("stroke-width", "0.02em")
          .attr("stroke", "black");

      const cols = [ "ZONE", "QTY", "CORES", "MEMORY" ];
      $(cols).each(function (d, text) {
        chgSet.append("text")
            .attr("class", "header")
            .attr("text-anchor", "middle")
            .attr("dx", 10 + (25 * d))
            .attr("dy", -7)
            .text(function(d) { return text; });
      });


       //   <image x="10" y="20" width="80" height="80" xlink:href="recursion.svg" />
//      chgSet.append("image")
//          .attr("x", -60)
//          .attr("y", -20)
//          .attr("width", 120)
//          .attr("height", 120 * (66/312))
//          .attr("xlink:href", "images/canzea_logo.svg");

  }
}

export default TableShape;


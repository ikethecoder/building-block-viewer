
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

import dutil from './2dUtils'

class SectionLeft {

  build(vis, domainType) {
      var twodUtils = new dutil()

      let obj = vis.append("g");

      obj.attr("transform", "translate(402, 266)")
      obj.classed("_" + domainType, true);


      // Green pie section
      twodUtils.buildLevel1(obj, 60, 60, false, "", {x:80,y:0});



      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 130)
          .attr("dy", 0)
          .text(function(d) { return "Applications"; });


      // Applications
      obj.append("circle")
      .attr("r", 24)
      .attr("stroke", "white")
      .attr("stroke-width", "0em")
      .attr("fill", "#CCCCCC")
      .attr("transform", "translate("+80+","+0+")")

      obj.append("text")
          .attr("class", "iconText")
          .attr("text-anchor", "middle")
          .attr("dx", 80)
          .attr("dy", 0)
          .style("font-size", "140%")
          .text(function(d) { var hex = "f1ae"; return unescape('%u' + hex); });

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 80)
          .attr("dy", 12)
          .text(function(d) { return "Customer"; });


//      twodUtils.buildLevel2(obj, 1, 50, 30, ["UI Services"], 170, {x:80,y:0},82);
//      twodUtils.buildLevel5(obj, 1, 170, 50, 30, true, {x:80,y:0}, 80);
//      twodUtils.buildLevel4Lines(obj, 1, 170, 50, 30, true, {x:80,y:0}, 80);

      twodUtils.buildLevel2(obj, 1, 60, 60, [], 110, {x:80,y:0}, 82);
      twodUtils.buildLevel5(obj, 1, 112, 60, 60, true, {x:80,y:0}, 80);
      twodUtils.buildLevel4Lines(obj, 1, 112, 60, 60, true, {x:80,y:0}, 80);


      // Green pie section
      // Resources

      let _offset = {x:355,y:220}

//      obj.append("line")
//          .attr("x1", (_offset.x))
//          .attr("y1", _offset.y)
//          .attr("x2", (_offset.x - 40))
//          .attr("y2", (_offset.y))
//          .attr("stroke", "black")
//          .attr("stroke-width", "0.05em");

      obj.append("line")
          .attr("x1", (_offset.x))
          .attr("y1", _offset.y)
          .attr("x2", (_offset.x))
          .attr("y2", (_offset.y - 130))
          .attr("stroke", "black")
          .attr("stroke-width", "0.05em");


      obj.append("circle")
      .attr("r", 20)
      .attr("stroke", "white")
      .attr("stroke-width", "0em")
      .attr("fill", "#CCCCCC")
      .attr("transform", "translate("+(_offset.x)+","+_offset.y+") translate(0,0)")

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", (_offset.x))
          .attr("dy", _offset.y)
          .attr("transform", "translate(0,0)")
          .text(function(d) { return "Resources"; });
  }
}

export default SectionLeft;


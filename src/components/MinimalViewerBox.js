import React, {PropTypes} from 'react';

class MinimalViewerBox extends React.Component {
  constructor(props, context) {
    super(props, context);

  }

  render() {
    const {appState} = this.props;

    return (
      <div>Hello World</div>
    );
  }

}

MinimalViewerBox.propTypes = {
  appState: PropTypes.object.isRequired
};

export default MinimalViewerBox;

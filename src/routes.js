import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import Viewer from './components/ViewerEcosystem.js';
import HomePage from './components/HomePage.js';
import NotFoundPage from './components/NotFoundPage.js';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="/viewer" component={Viewer} />
    <Route path="*" component={NotFoundPage} />
  </Route>
);

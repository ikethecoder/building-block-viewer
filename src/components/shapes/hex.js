
import d3 from 'd3';
import React, {PropTypes} from 'react';

class HexShape {

  splitCaption(caption) {
    var MAXIMUM_CHARS_PER_LINE = 6;

    var words = caption.split(' ');
    var line = "";

    var lines = [];
    for (var n = 0; n < words.length; n++) {
        var testLine = line + words[n] + " ";
        if (testLine.length > MAXIMUM_CHARS_PER_LINE && line.length > 0)
        {
            lines.push(line);

            line = words[n] + " ";
        }
        else {
            line = testLine;
        }
    }
    lines.push(line);

    return lines;
  }

  polygon(x,y,rad,sides) {
    var path = "M ";
    for (var a=0;a<sides;a++) {
      if (a>0) {path = path + "L ";}
      path = path + (x+(Math.sin(2*Math.PI*a/sides)*rad)) + " " + (y-(Math.cos(2*Math.PI*a/sides)*rad)) + " ";
    }
    path = path + "z";
    return path;
  }

  hexagon(x,y,r) {
    var x1 = x;
    var y1 = y-r;
    var x2 = x+(Math.cos(Math.PI/6)*r);
    var y2 = y-(Math.sin(Math.PI/6)*r);
    var x3 = x+(Math.cos(Math.PI/6)*r);
    var y3 = y+(Math.sin(Math.PI/6)*r);
    var x4 = x;
    var y4 = y+r;
    var x5 = x-(Math.cos(Math.PI/6)*r);
    var y5 = y+(Math.sin(Math.PI/6)*r);
    var x6 = x-(Math.cos(Math.PI/6)*r);
    var y6 = y-(Math.sin(Math.PI/6)*r);

    var path = "M"+x1+" "+y1+" L"+x2+" "+y2+" L"+x3+" "+y3+" L"+x4+" "+y4+" L"+x5+" "+y5+" L"+x6+" "+y6+"z";
    return path;
  }

  build(newNodes, domainType) {
      const self = this;
      const _s32 = (Math.sqrt(3)/2);
      const A = 25;
      const xDiff = 0;
      const yDiff = 0;
      const pointData = [
          [A+xDiff, 0+yDiff],
          [A/2+xDiff, (A*_s32)+yDiff],
          [-A/2+xDiff, (A*_s32)+yDiff],
          [-A+xDiff, 0+yDiff],
          [-A/2+xDiff, (-A*_s32)+yDiff],
          [A/2+xDiff, (-A*_s32)+yDiff]];//,
         // [A+xDiff, 0+yDiff]];

      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      var line = d3.svg.line()
          .x(function(d) { return d.x; })
          .y(function(d) { return d.y; })
          .interpolate("basis");

      chgSet.selectAll("path.area")
          .data([pointData]).enter().append("path")
//          .attr("stroke-linejoin", "round")
//          .attr("stroke-linecap", "round")
          .attr("stroke", "5")
          .attr("d", d3.svg.line().interpolate("cardinal-closed"))
          //.attr("d", function (d) { console.log("d = "+d); return d3.svg.line(d); /*self.polygon(5, 5, 40, 8); */} )
//          .attr("d", "M5.000000000000001 95.26279441628824Q0 86.60254037844386 5.000000000000001 77.94228634059948L45 8.660254037844386Q50 0 60 0L140 0Q150 0 155 8.660254037844386L195 77.94228634059948Q200 86.60254037844386 195 95.26279441628824L155 164.54482671904333Q150 173.20508075688772 140 173.20508075688772L60 173.20508075688772Q50 173.20508075688772 45 164.54482671904333Z")
      ;

      const textNodes = chgSet
          .append("text")
          .attr("text-anchor", "middle")
          //.attr("dominant-baseline", "central")
          .attr("dx", 0)
          .attr("dy", ".20em");

      textNodes.append("tspan")
            .attr("x", 0)
            .attr("y", 0)
            .text(function (d) { const lines = self.splitCaption(d.name); return lines[0]; });

      textNodes.append("tspan")
            .attr("x", 0)
            .attr("y", 15)
            .text(function (d) { const lines = self.splitCaption(d.name); return (lines.length == 1 ? "":lines[1]); });

  }
}

export default HexShape;


import React, {PropTypes} from 'react';

import $ from 'jquery';

import ReactDOM from 'react-dom';

import ViewerBox from './ViewerBox.js';
import ViewerCanvas from './ViewerCanvas.js';

class Viewer extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.save = this.save.bind(this);
    this.onTimeframeChange = this.onTimeframeChange.bind(this);
    this.fuelSavingsKeypress = this.fuelSavingsKeypress.bind(this);
  }

  fuelSavingsKeypress(name, value) {
    this.props.calculateFuelSavings(this.props.appState, name, value);
  }

  save() {
    //this.props.saveFuelSavings(this.props.appState);
      const obj = ReactDOM.findDOMNode(this);

      const width = obj.offsetWidth;
      console.log("Width: " + width);

  }

  componentDidMount() {
      const obj = ReactDOM.findDOMNode(this);

      console.log("id = "+obj.id);
      const width = obj.offsetWidth;
      console.log("Width: " + width);

      //this.loadDataFromServer();

      var el = document.createElement( 'i' );
      $(el).addClass("fa fa-home");

      //el.getElementsByTagName( 'a' );

      //var el = $(document.body).append( '<i class="fa fa-home"></i>' );
      //var el = $(document.body).append( '<i class="fa fa-home"></i>' );
      console.log("Result: " + $(el).html().length);

      const FA_GROUP = "&#xf0c0";
      const g = {
        "nodes":
[
  {
    "name": "COM01",
    "title": {
      "label": "Pushes code"
    },
    "x": 97,
    "y": 113,
    "type": "comment",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "PE",
    "title": {
      "icon": "&#xf0c0",
      "label": "Team"
    },
    "x": 90,
    "y": 62,
    "type": "icon",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "INT",
    "title": {
      "icon": "&#xf0c1",
      "label": "Integration"
    },
    "x": 725,
    "y": 457,
    "type": "icon",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CUST",
    "title": {
      "icon": "&#xf1ae",
      "label": "Customer"
    },
    "x": 723,
    "y": 389,
    "type": "icon",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CANZEA",
    "title": {
      "role": "Ecosystem Management",
      "solution": "Canzea"
    },
    "x": 289,
    "y": 44,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "DO",
    "title": {
      "role": "Provisioning",
      "solution": "Digital Ocean"
    },
    "x": 532,
    "y": 44,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "OS",
    "title": {
      "role": "Operating System",
      "solution": "CentOS"
    },
    "x": 645,
    "y": 45,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "GL",
    "title": {
      "role": "Source Control",
      "solution": "GitLab"
    },
    "x": 193,
    "y": 121,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CD",
    "title": {
      "role": "Delivery Pipeline",
      "solution": "Go.CD"
    },
    "x": 392,
    "y": 121,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "RC",
    "title": {
      "role": "Collaboration",
      "solution": "Rocket.Chat"
    },
    "x": 192,
    "y": 177,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "MV",
    "title": {
      "role": "Build",
      "solution": "Maven"
    },
    "x": 458,
    "y": 191,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "DK",
    "title": {
      "role": "Container Engine",
      "solution": "Docker"
    },
    "x": 567,
    "y": 223,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JV",
    "title": {
      "role": "Container Engine",
      "solution": "Java"
    },
    "x": 567,
    "y": 269,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "DKR",
    "title": {
      "role": "Artifact Registry",
      "solution": "Docker"
    },
    "x": 665,
    "y": 224,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JVR",
    "title": {
      "role": "Artifact Registry",
      "solution": "Archiva"
    },
    "x": 666,
    "y": 270,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "AN",
    "title": {
      "role": "Deployment",
      "solution": "Ansible"
    },
    "x": 333,
    "y": 191,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "SE",
    "title": {
      "role": "Ops Alerting",
      "solution": "Sensu"
    },
    "x": 192,
    "y": 240,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "ELK",
    "title": {
      "role": "Ops Monitoring",
      "solution": "ELK"
    },
    "x": 80,
    "y": 240,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "CN",
    "title": {
      "role": "Configuration Mgmt",
      "solution": "Consul"
    },
    "x": 396,
    "y": 402,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "KG",
    "title": {
      "role": "API Management",
      "solution": "Kong"
    },
    "x": 489,
    "y": 447,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "VA",
    "title": {
      "role": "Key Management",
      "solution": "Hashicorp Vault"
    },
    "x": 285,
    "y": 401,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "LETS",
    "title": {
      "role": "SSL Cert Provider",
      "solution": "LetsEncrypt"
    },
    "x": 285,
    "y": 447,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "NG",
    "title": {
      "role": "Load Balancing",
      "solution": "NGINX"
    },
    "x": 490,
    "y": 402,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "MG",
    "title": {
      "role": "Document Database",
      "solution": "MongoDB"
    },
    "x": 396,
    "y": 445,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "RAB",
    "title": {
      "role": "Messaging",
      "solution": "RabbitMQ"
    },
    "x": 396,
    "y": 489,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA01",
    "title": {
      "solution": "React"
    },
    "x": 50,
    "y": 387,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA02",
    "title": {
      "solution": "Redux"
    },
    "x": 114,
    "y": 410,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA03",
    "title": {
      "solution": "Babel"
    },
    "x": 51,
    "y": 412,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA04",
    "title": {
      "solution": "Webpack"
    },
    "x": 114,
    "y": 433,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA05",
    "title": {
      "solution": "Browsersync"
    },
    "x": 50,
    "y": 434,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA06",
    "title": {
      "solution": "Mocha"
    },
    "x": 113,
    "y": 456,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA07",
    "title": {
      "solution": "Isparta"
    },
    "x": 50,
    "y": 456,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA08",
    "title": {
      "solution": "TrackJS"
    },
    "x": 111,
    "y": 480,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA09",
    "title": {
      "solution": "ESLint"
    },
    "x": 51,
    "y": 480,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA10",
    "title": {
      "solution": "SASS"
    },
    "x": 111,
    "y": 503,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "REA11",
    "title": {
      "solution": "npm"
    },
    "x": 50,
    "y": 503,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV01",
    "title": {
      "solution": "java"
    },
    "x": 184,
    "y": 454,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV02",
    "title": {
      "solution": "spring"
    },
    "x": 184,
    "y": 501,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV03",
    "title": {
      "solution": "selenium"
    },
    "x": 184,
    "y": 477,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "TA",
    "title": {
      "role": "Test Automation",
      "solution": ""
    },
    "x": 556,
    "y": 120,
    "type": "buildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "TA01",
    "title": {
      "solution": "Accessibility"
    },
    "x": 686,
    "y": 164,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "TA02",
    "title": {
      "solution": "Functional"
    },
    "x": 751,
    "y": 139,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "TA03",
    "title": {
      "solution": "Stress"
    },
    "x": 750,
    "y": 163,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "TA04",
    "title": {
      "solution": "Load"
    },
    "x": 686,
    "y": 139,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "TA05",
    "title": {
      "solution": "Soak"
    },
    "x": 750,
    "y": 113,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "TA06",
    "title": {
      "solution": "Spike"
    },
    "x": 686,
    "y": 113,
    "type": "stackSolution",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "LOGO",
    "title": {
      "image": "images/canzea-managedby.svg",
      "width": 140,
      "height": 20.95295980409741
    },
    "x": 649,
    "y": 500,
    "type": "image",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "Apps1",
    "title": {
      "role": "Browser",
      "solution": ""
    },
    "x": 601,
    "y": 405,
    "type": "stackedBuildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "Apps2",
    "title": {
      "role": "Mobile",
      "solution": ""
    },
    "x": 602,
    "y": 445,
    "type": "stackedBuildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "Apps3",
    "title": {
      "role": "Services",
      "solution": ""
    },
    "x": 601,
    "y": 485,
    "type": "stackedBuildingBlock",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvBuild",
    "title": {
      "label": "Build",
      "pipeline":false,
      "order": { "product":"I4", "qty":1}
    },
    "x": 110,
    "y": 320,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvPerf",
    "title": {
      "label": "Performance",
      "pipeline":false,
      "order": { "product":"I2", "qty":1}
    },
    "x": 235,
    "y": 320,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvInt",
    "title": {
      "label": "Integration",
      "pipeline":true,
      "order": { "product":"I1", "qty":1}
    },
    "x": 375,
    "y": 320,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvUser",
    "title": {
      "label": "User Acceptance",
      "pipeline":true,
      "order": { "product":"I2", "qty":1}
    },
    "x": 510,
    "y": 320,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "EnvLive",
    "title": {
      "label": "Live",
      "pipeline":true,
      "order": { "product":"I2", "qty":1}
    },
    "x": 645,
    "y": 320,
    "type": "table",
    "fixed": true,
    "boundary": "A"
  }
]
        ,
        "links": [
        ]
      }

      const links = [
        {"source":"CANZEA", "target":"DO", "type":"straight"},
        {"source":"CANZEA", "target":"GL", "type":"straight"},
        {"source":"GL", "target":"CD", "type":"straight"},
        {"source":"PE", "target":"GL", "type":"straight", "label": "Pushes code"},
        {"source":"PE", "target":"RC", "type":"straight"},
        {"source":"CD", "target":"MV", "type":"straight"},
        {"source":"MV", "target":"DK", "type":"straight"},
        {"source":"MV", "target":"JV", "type":"straight"},
        {"source":"JV", "target":"JVR", "type":"straight"},
        {"source":"DK", "target":"DKR", "type":"straight"},
        {"source":"CD", "target":"AN", "type":"straight"},
        {"source":"CD", "target":"DO", "type":"straight"},
        {"source":"CD", "target":"TA", "type":"straight"},
        {"source":"SE", "target":"RC", "type":"straight"},
        {"source":"PE", "target":"ELK", "type":"straight"},
        {"source":"DO", "target":"OS", "type":"straight"}

//        {"source":"Digital", "target":"IDE", "type":""},
////        {"source":"Digital", "target":"UTE", "type":""},
////        {"source":"Digital", "target":"Perf", "type":""},
////        {"source":"Digital", "target":"Live", "type":""},
//        {"source":"App-1", "target":"MongoDB", "type":""},
//        {"source":"App-1", "target":"Go.CD Agent_2", "type":""},
//        {"source":"App-1", "target":"etcd_2", "type":""},
//        {"source":"App-1", "target":"Kong", "type":""},
//        {"source":"App-1", "target":"NGINX", "type":""},
//        {"source":"CD", "target":"GitLab", "type":""},
//        {"source":"CD", "target":"etcd", "type":""},
//        {"source":"CD", "target":"Go.CD", "type":""},
//        {"source":"CD", "target":"Ansible", "type":""},
//        {"source":"CD", "target":"Go.CD Agent", "type":""},
//        {"source":"etcd", "target":"do-sf-1", "type":""},
//        {"source":"etcd", "target":"do-sf-2", "type":""},
//        {"source":"etcd", "target":"do-sf-4", "type":""},
//        {"source":"etcd", "target":"do-sf-5", "type":""},
//        {"source":"etcd", "target":"do-sf-6", "type":""},
//        {"source":"etcd", "target":"do-sf-7", "type":""},
//        {"source":"etcd", "target":"do-sf-8", "type":""},
//        {"source":"etcd", "target":"do-sf-9", "type":""},
//        {"source":"etcd", "target":"do-sf-10", "type":""},
//        {"source":"etcd", "target":"do-sf-3", "type":""}
      ];

      this.refs.canvas.setState({graph: g, links: links});
      this.setState({graph: g, links: links});

      const self = this;
      $(window).resize(function (e) {
          self.refs.canvas.refreshSize();
      });

  }

  onTimeframeChange(e) {
    //this.props.calculateFuelSavings(this.props.appState, 'milesDrivenTimeframe', e.target.value);
  }


  loadDataFromServer() {
      let self = this;

      const eid = '572f9954d4c67f8e5573d7dd';

      console.log("loading data from server...");
      const url = "http://localhost:8888/api/saasexpress/platform/" + eid;
      let request = $.get(url, function (response) {

          let links = response.links;
          response.links = [];

          //self.graph = response;
          //self.refs.canvas.setGraph(response);
          //self.setState(response);
          //self.recalcSpace();
          self.refs.canvas.setState({"graph":response, "links":links});
          //self.refs.canvas.update();
//
//          if (links) {
//              for (var i = 0 ; i < links.length; i++) {
//                  self.refs.canvas.addLink(links[i].source, links[i].target, links[i].type);
//              }
//
//          }
      });
  }

//      <div style={{width:"9933px",height:"7017px"}} className="canvas">

  render() {
    let ratio = 0.70643; // A1 ratio
    ratio = 0.666666; // 24x36 poster
    let w = 800;
    let h = (w * ratio);

    let margin = 10;

    const zones = [
        { "type" : "swimlane", "rectangle": [margin, margin, w - (2*margin), h - (2*margin)], "title": "Software Ecosystem Blue Print" },
        { "type" : "applicationStack", "rectangle": [340, 360, 200, 160], "title": "Application Stack" },
        { "type" : "applicationStack", "rectangle": [550, 360, 95, 160], "title": "Applications" },
        { "type" : "segment", "rectangle": [650, 85, 135, 100], "title": "Testing Stack" },
        { "type" : "segment", "rectangle": [15, 360, 130, 160], "title": "Browser Stack" },
        { "type" : "segment", "rectangle": [146, 360, 80, 160], "title": "Java Stack" },
        { "type" : "securityStack", "rectangle": [235, 360, 100, 160], "title": "Security" }
        //{ "type" : "environment", "rectangle": [10, 300, 600, 160], "title": "Environments" }
    ];

    return (
      <div className="canvas">
        <ViewerCanvas ref="canvas" zones={zones} graph="{this.graph}"/>
      </div>
    );
  }
          /*
          <pre style={{display:"none"}}>{JSON.stringify(this.state, null, 2)}</pre>

          <div style={{padding:"10px"}}>
            <img src="images/canzea_logo.svg" style={{width:"400px"}}/>
          </div>

          <input type="submit" value="Save" onClick={this.save}/>
           */
}


Viewer.propTypes = {
};

export default Viewer;

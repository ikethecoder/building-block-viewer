
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

class MultiItemShape {

  build(vis, domainType) {

      var x;
      var y;
      var A;

      let obj = vis.append("g");

      obj.attr("transform", "translate(400, 260)")
      obj.classed("_" + domainType, true);


      // SOURCE
      obj.append("circle")
      .attr("r", 24)
      .attr("stroke", "white")
      .attr("stroke-width", "0em")
      .attr("fill", "#CCCCCC")
      .attr("transform", "translate("+-80+","+0+")")


      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", -130)
          .attr("dy", 0)
          .text(function(d) { return "Code"; });
      buildLevel2(4, 225, 90, ["Mobile Stack","React Stack","Java Stack","Configuration"], 160, {x:-80,y:0});
      buildLevel4(4, 160, 225, 90, false, {x:-80,y:0});

      var tiles = [
            {
              "name": "REA01",
              "title": {
                "solution": "React"
              },
              "x": 85,
              "y": 270,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA02",
              "title": {
                "solution": "Redux"
              },
              "x": 85,
              "y": 288,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA03",
              "title": {
                "solution": "Babel"
              },
              "x": 127,
              "y": 323,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA04",
              "title": {
                "solution": "Webpack"
              },
              "x": 85,
              "y": 305,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA05",
              "title": {
                "solution": "Browsersync"
              },
              "x": 43,
              "y": 287,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA06",
              "title": {
                "solution": "Mocha"
              },
              "x": 85,
              "y": 323,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA07",
              "title": {
                "solution": "Isparta"
              },
              "x": 44,
              "y": 305,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA08",
              "title": {
                "solution": "TrackJS"
              },
              "x": 127,
              "y": 305,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA09",
              "title": {
                "solution": "ESLint"
              },
              "x": 127,
              "y": 270,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA10",
              "title": {
                "solution": "SASS"
              },
              "x": 126,
              "y": 287,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            },
            {
              "name": "REA11",
              "title": {
                "solution": "npm"
              },
              "x": 43,
              "y": 270,
              "type": "stackSolutionMicro",
              "fixed": true,
              "boundary": "A"
            }
      ]
      buildMatrix(4, 160, 225, 90, false, {x:-80,y:0}, 82, 0, tiles);

      tiles = [
  {
    "name": "JAV01",
    "title": {
      "solution": "java"
    },
    "x": 70,
    "y": 208,
    "type": "stackSolutionMicro",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV02",
    "title": {
      "solution": "spring"
    },
    "x": 124,
    "y": 225,
    "type": "stackSolutionMicro",
    "fixed": true,
    "boundary": "A"
  },
  {
    "name": "JAV03",
    "title": {
      "solution": "selenium"
    },
    "x": 124,
    "y": 207,
    "type": "stackSolutionMicro",
    "fixed": true,
    "boundary": "A"
  }
      ]
      buildMatrix(4, 160, 225, 90, false, {x:-80,y:0}, 82, 1, tiles);

      tiles = [
  {
    "name": "JAV01",
    "title": {
      "solution": "java"
    },
    "x": 70,
    "y": 208,
    "type": "stackSolutionMicro",
    "fixed": true,
    "boundary": "A"
  }

      ]
      buildMatrix(4, 160, 225, 90, false, {x:-80,y:0}, 82, 2, tiles);
      buildMatrix(4, 160, 225, 90, false, {x:-80,y:0}, 82, 3, tiles);


      function buildLevel2(segments, base, range, _names, outer, delta, inner=82) {


        for ( var x = 0; x < segments; x++) {

          var start = base + (x * range / segments);
          var end = base + ((x+1) * range / segments) - 0.0;
          var arc3 = d3.svg.arc()
              .innerRadius(inner)
              .outerRadius(outer)
              .startAngle(start * (Math.PI/180)) //converting from degs to radians
              .endAngle(end * (Math.PI/180)) //just radians

          obj.append("path")
            .attr("d", arc3)
            .attr("stroke-width", "0.0em")
            .attr("stroke", "white")
            .attr("transform", "translate(" + delta.x+","+delta.y+")")
            .attr("fill", (base == 315 || base == 135 ? "#e2cc93":"#9fcefe"))

        }

        var radius = 90;
        for ( var s = 0; s < segments; s++) {
            let mid = (range/segments) / 2;
            let rotation = (base - 90 + mid + (s * range/segments));// * (Math.PI/180);

            var A = rotation * (Math.PI/180);

            if (rotation > 130 && rotation <= 220) {
              rotation = rotation - 180;
              //A = (rotation+180) * (Math.PI/180);
              x = (radius + 50) * Math.cos(A);
              y = (radius + 50) * Math.sin(A);
              const _name = _names[s];
              obj.append("text")
                  .attr("class", "title")
                  .attr("text-anchor", "left")
                  .attr("dx", 0)
                  .attr("dy", 0)
                  .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
                  //.attr("transform", "translate(" + delta.x+","+delta.y+")")
                  .text(function(d) { return _name; });
            } else if ((rotation > 45 && rotation <= 130) || (rotation < -45 || rotation > 220)) {
              rotation = 0;
              //A = (rotation+180) * (Math.PI/180);
              x = (radius + 50) * Math.cos(A);
              y = (radius + 50) * Math.sin(A);
              const _name = _names[s];
              obj.append("text")
                  .attr("class", "title")
                  .attr("text-anchor", "middle")
                  .attr("dx", 0)
                  .attr("dy", 0)
                  .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
                  //.attr("transform", "translate(" + delta.x+","+delta.y+")")
                  .text(function(d) { return _name; });
            } else {
              x = (radius + 20) * Math.cos(A);
              y = (radius + 20) * Math.sin(A);
              const _name = _names[s];
              obj.append("text")
                  .attr("class", "title")
                  .attr("text-anchor", "left")
                  .attr("dx", 0)
                  .attr("dy", 0)
                  .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
                  .text(function(d) { return _name; });
            }

                ///translate(" + x+","+y+")

        }
      }

      function buildLevel2Labels(segments, base, range, _names, outer, delta, inner=82) {


        var radius = 100;
        for ( var s = 0; s < segments; s++) {
            let mid = (range/segments) / 2;
            let rotation = (base - 90 + mid + (s * range/segments));// * (Math.PI/180);

            A = rotation * (Math.PI/180);
            x = (radius + 20) * Math.cos(A);
            y = (radius + 20) * Math.sin(A);
            const _name = _names[s];
            obj.append("text")
                .attr("class", "title")
                .attr("text-anchor", "left")
                .attr("dx", 0)
                .attr("dy", 0)
                .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + 0 + ")")
                .text(function(d) { return _name + rotation; });

                ///translate(" + x+","+y+")

        }
      }



      function buildLevel4(segments, radius, base, range, side, delta, inner=82) {

        var extended = (side ? "350":"-370");

        for ( var s = 0; s < segments; s++) {

      //    var start = base + (s * range / segments);
      //    var end = base + ((s+1) * range / segments) - 0.5;
      //    var arc3 = d3.svg.arc()
      //        .innerRadius(182)
      //        .outerRadius(210)
      //        .startAngle(start * (Math.PI/180)) //converting from degs to radians
      //        .endAngle(end * (Math.PI/180)) //just radians
      //
      //    obj.append("path")
      //      .attr("d", arc3)
      //      .attr("fill", "#8cb7e2")

          var A = (base - 90 + (s * range/segments)) * (Math.PI/180);
          var x = (radius) * Math.cos(A);
          var y = (radius) * Math.sin(A);

          var A1 = (base - 90 + ((s+1) * range/segments)) * (Math.PI/180);
          var x1 = (radius) * Math.cos(A1);
          var y1 = (radius) * Math.sin(A1);
          var height = y1-y; //y1-y;//Math.sqrt(Math.pow(s * range/segments, 2)/2);

          //A160,160 0 0,1 -72.63847995832751,-142.56104387013886
          var cx = radius;
          var cy = radius;
          var wid = extended - delta.x;
//          obj.append("path")
//            .attr("d", "M" + x+","+y+" A" + cx + "," + cy + " 0 0,1 " + (x1) + "," + (y+height) + " L" + wid + "," + (y+height) + " L" + wid+","+y)
//            .attr("stroke-width", "0.0em")
//            .attr("stroke", "white")
//            .attr("fill", "#8cb7e2")
//            .attr("transform", "translate("+delta.x+","+delta.y+")")

        }

        radius = radius - 20;
        for ( s = 0; s < segments; s++) {

            var start = base + (s * range / segments);
            var end = base + ((x+1) * range / segments);
            A = (base - 90 + (s * range/segments)) * (Math.PI/180);
            x = (radius + 20) * Math.cos(A);
            y = (radius + 20) * Math.sin(A);

            var A0 = (45 - 90 + (s * range/segments)) * (Math.PI/180);
            var x0 = inner * Math.cos(A);
            var y0 = inner * Math.sin(A);

            obj.append("path")
                .attr("d", "M" + x0 + ","+ y0 + "L" + (x) + ","+(y)+" L" + extended+"," + y + "")
                .attr("stroke-width", "0.07em")
                .attr("stroke", "white")
                .attr("fill", "none")
                .attr("transform", "translate("+delta.x+","+delta.y+")")

        }
      }


      function buildMatrix (segments, radius, base, range, side, delta, inner=82, s, tiles) {
          var extended = (side ? "350":"-370");

          // fill the space
          // Find whether it is divisible by 5,3,2,1 and fill the space accordingly


          var tileCount = tiles.length;
          var iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
          if (iMax == tileCount) {
              tileCount++;
              iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
              tileCount--;
          }
          var dvMax = Math.ceil(tileCount / iMax)
          var tile = tiles.length - 1;

          var width = 170/dvMax;//Math.round((baseX - (extended - delta.x)) / iMax);

          for (var i = 0; i < iMax; i++) {

              for (var dv = 0; dv < dvMax; dv++) {
                  // We need to calculate two points
                  // One at the base angle and the other at the other angle

                  var A = (base - 90 + ((s+((i+1)*(1/iMax))) * range/segments)) * (Math.PI/180);
                  var x = (radius) * Math.cos(A) - 1;
                  var y = (radius) * Math.sin(A);

                  var A1 = (base - 90 + ((s+(i*(1/iMax))) * range/segments)) * (Math.PI/180);
                  var x1 = (radius) * Math.cos(A1) - 1;
                  var y1 = (radius) * Math.sin(A1);

                  var cx = radius;
                  var cy = radius;

                  if (dv == 0) {
                      obj.append("path")
                        .attr("d", "M" + x+","+y+" A" + cx + "," + cy + " 0 0,0 " + (x1) + "," + y1 +" L" + (x1-width) + "," + (y1) + " A" + cx + "," + cy + " 0 0,1 " + (x-width) + "," + (y) + " L" + (x) + "," + (y))
                        .attr("stroke-width", "0.05em")
                        .attr("stroke", "white")
                        .attr("fill", "#8cb7e2")
                        .attr("transform", "translate("+delta.x+","+delta.y+")")
                  } else {
                      var shift = (dv * (width+(x1-x) + 5));
                      x = x - (dv*(width+0))
                      x1 = x1 - (dv*(width+0));
                      obj.append("path")
                        .attr("d", "M" + x+","+y+" A" + cx + "," + cy + " 0 0,0 " + (x1) + "," + y1 +" L" + (x1-width) + "," + (y1) + " A" + cx + "," + cy + " 0 0,1 " + (x-width) + "," + (y) + " L" + (x) + "," + (y))
                        .attr("stroke-width", "0.05em")
                        .attr("stroke", "white")
                        .attr("fill", "#8cb7e2")
                        .attr("transform", "translate("+delta.x+","+delta.y+")")
                  }
                  var _name = tiles[tile].title.solution;//tiles[(dv*iMax) + i]['name'];
                  obj.append("text")
                      .attr("class", "title")
                      .attr("text-anchor", "middle")
                      .attr("dominant-baseline", "central")
                      .attr("dx", 0)
                      .attr("dy", 0)
                      .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + (((x+x1)/2)-(width/2))+","+(y+((y1-y)/2))+") rotate(" + 0 + ")")
                      .text(function(d) { return _name; });

                  tile--;
                  if (tile < 0) {
                    break;
                  }
              }
              if (tile < 0) {
                break;
              }
          }
      }
  }
}

export default MultiItemShape;


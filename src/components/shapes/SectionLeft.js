
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

import dutil from './2dUtils'

class SectionLeft {

  build(vis, domainType) {
      var twodUtils = new dutil()

      let obj = vis.append("g");

      obj.attr("transform", "translate(402, 266)")
      obj.classed("_" + domainType, true);

      // Develop circle
      obj.append("circle")
      .attr("r", 24)
      .attr("stroke", "white")
      .attr("stroke-width", "0em")
      .attr("fill", "#CCCCCC")
      .attr("transform", "translate("+-80+","+0+")")



      obj.append("text")
          .attr("class", "iconText")
          .attr("text-anchor", "middle")
          .attr("dx", -80)
          .attr("dy", 0)
          .style("font-size", "120%")
          .text(function(d) { var hex = "f0c0"; return unescape('%u' + hex); });

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", -80)
          .attr("dy", 12)
          .text(function(d) { return "Team"; });

      twodUtils.buildLevel1(obj, 225, 90, false, "", {x:-80,y:0});


      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", -130)
          .attr("dy", -5)
          .text(function(d) { return "Development"; });

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", -130)
          .attr("dy", 5)
          .text(function(d) { return "Stacks"; });


      twodUtils.buildLevel2(obj, 3, 225, 90, ["React Stack","Java Stack", "Configuration"], 160, {x:-80,y:0});

      twodUtils.buildLevel4Lines(obj, 3, 160, 225, 90, false, {x:-80,y:0});


      // Green pie section
      // Resources

//      let _offset = {x:-355,y:220}
//
//      obj.append("line")
//          .attr("x1", (_offset.x))
//          .attr("y1", _offset.y)
//          .attr("x2", (_offset.x))
//          .attr("y2", (_offset.y - 40))
//          .attr("stroke", "black")
//          .attr("stroke-width", "0.05em");


//      obj.append("circle")
//      .attr("r", 20)
//      .attr("stroke", "white")
//      .attr("stroke-width", "0em")
//      .attr("fill", "#CCCCCC")
//      .attr("transform", "translate("+(_offset.x)+","+_offset.y+") translate(0,0)")
//
//      obj.append("text")
//          .attr("class", "title")
//          .attr("text-anchor", "middle")
//          .attr("dx", (_offset.x))
//          .attr("dy", _offset.y)
//          .attr("transform", "translate(0,0)")
//          .text(function(d) { return "Resources"; });

  }
}

export default SectionLeft;


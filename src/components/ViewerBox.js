import React, {PropTypes} from 'react';

import ViewerCanvas from './ViewerCanvas.js';

class ViewerBox extends React.Component {
  constructor(props, context) {
    super(props, context);
    //this.setState({nodes: [], links: [], initType: "", link: "", boundary:"A", name:"something"});
  }

  handleSave() {
    console.log("Saving...");
  }

  render() {
//    const {appState} = this.props;

    const graph = {
        "nodes":[],
        "links":[]
    };

    return (
        <div style={{width:"100%",height:"100%"}}>
            <ViewerCanvas ref="child" data={graph} eid={this.props.eid}/>
            <div>
                <button onClick={this.handleSave}>Save</button>
                <button onClick={this.handleAdd}>Add</button>
                <button onClick={this.handleDelete}>Delete</button>
            </div>
        </div>
    );
  }

}

ViewerBox.propTypes = {
//  appState: PropTypes.object.isRequired,
  eid: PropTypes.string.isRequired
//  name: PropTypes.object.isRequired
};

export default ViewerBox;

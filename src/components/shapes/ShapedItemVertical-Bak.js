
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

class MultiItemShape {

  build(newNodes, domainType) {
      self = this;
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      var obj = chgSet;//.append("g");//.attr("transform", "translate(" + 80 +","+0+")");


      var s = 0, segments = 4, radius = 160, base = 170, range = 90, side = false, delta = {x:0,y:0}, inner=82


for (var q = 0; q < 2; q++) {
    for (var z = 1; z < 4; z++) {
          obj.append("path")
              .attr("stroke-width", "0.05em")
              .attr("stroke", "#8cb7e2")
              .attr("fill", "#white")
              .attr("transform", "translate("+delta.x+","+delta.y+")")
              .attr("d", function(d) {
                  if (d.title.segment == "ReactStack") {
                    //s = 0, segments = 4, radius = 160, base = 225, range = 90, side = false, inner=82
                  } else {
                    s = 0, segments = 1, radius = 200, base = 130, range = 180, side = false, inner=82
                  }
                  var loc = self.calculateTilePosition(d.layout.seq, d.layout.total);

                  var i = loc.row;
                  var dv = loc.col;
                  var width = loc.width;
                  var iMax = loc.maxRow;

                  var b0 = self.calculateCoordinate(radius, base + 20 + (z*20)); //((0 + (1*(s+((loc.row+1)*(1/(loc.maxRow+0)))))) * range/segments)
                  var b1 = self.calculateCoordinate(radius, base + (z*20)); //((0 + (1*(s+((loc.row+1)*(1/(loc.maxRow+0)))))) * range/segments)

                  var c0 = self.calculateCoordinate(radius+(q*50), base + (z*20)); //((0 + (1*(s+((loc.row+1)*(1/(loc.maxRow+0)))))) * range/segments)
                  var c1 = self.calculateCoordinate(radius+(q*50), base + 20+((z+0)*20) );

                  var c2 = self.calculateCoordinate(radius + 50 + (q*50), base + (z*20));
                  var c3 = self.calculateCoordinate(radius + 50 + (q*50), base + 20+(z*20) );

                  var cx = radius;
                  var cy = radius;

                  if (dv != 0) {
                      //var shift = (dv * (width+(x1-x) + 5));
                      //x = x - (dv*(width+0))
                      //x1 = x1 - (dv*(width+0));
                  }

                                radius = 200;

                                  cx = 500;
                                  cy = 500;

                  var coords = self.focusAt (b1.x,c0.y, b0.x,c1.y, b0.x,c3.y, b1.x,c2.y);


                  var dat = d3.select(this.parentNode).datum();

                  dat.x = 400 + coords[8];
                  dat.y = 200 + coords[9];



                  //return "M" + (x)+","+y+" A" + radius + "," + radius + " 0 0,0 " + (x1) + "," + y + " L" + (x1+slant) + "," + (height) + " A" + cx + "," + cy + " 0 0,1 " + (x-slant) + "," + height +" L" + (x) + "," + y + ")"

                  return "M" + coords[0]+","+coords[1]+" A" + radius + "," + radius + " 0 0,1 " + coords[2] + "," + coords[3] +" L" + coords[4] + "," + coords[5] + " A" + cx + "," + cy + " 0 0,0 " + coords[6] + "," + coords[7] + " L" + coords[0] + "," + coords[1];
              })
    }
}

//      obj.append("text")
//          .attr("class", "title")
//          .attr("text-anchor", "middle")
//          .attr("dominant-baseline", "central")
//          .attr("dx", 0)
//          .attr("dy", 0)
//          .attr("transform", function(d) {
//              if (d.title.segment == "ReactStack") {
//                ///s = 1, segments = 4, radius = 160, base = 225, range = 90, side = false, inner=82
//              } else {
//                s = 2, segments = 4, radius = 125, base = 90, range = 180, side = false, inner=82
//              }
//
//              var loc = self.calculateTilePosition(d.layout.seq, d.layout.total);
//
//              var i = loc.row;
//              var dv = loc.col;
//              var width = loc.width;
//              var iMax = loc.maxRow;
//
//              var c0 = self.calculateCoordinate(radius, base + ((s+((loc.row+1)*(1/loc.maxRow))) * range/segments) );
//              var x = c0.x;
//              var y = c0.y;
//
//              var c1 = self.calculateCoordinate(radius, base + ((s+((loc.row)*(1/loc.maxRow))) * range/segments) );
//              var x1 = c1.x;
//              var y1 = c1.y;
//
//              if (dv != 0) {
//                  var shift = (dv * (width+(x1-x) + 5));
//                  x = x - (dv*(width+0))
//                  x1 = x1 - (dv*(width+0));
//              }
//              var coords = self.focusAt (x,y, x1,y1, x1,y1-50, x,y1-50);
//              x = ((coords[0] + coords[2])/2) - width/2;
//              y = coords[1] + (coords[3]-coords[1])/2;
//
//              return "translate(" + delta.x+","+delta.y+") translate(" + x+","+y+")"
//          })
//          .text(function(d) { return d.title.solution; });

  }

  calculateDims () {
  }

  focusAt (x1,y1,x2,y2,x3,y3,x4,y4) {
      var coords = []
      var lowPoint = {x:0, y:0 }
      coords.push(x1 - lowPoint.x);
      coords.push(y1 - lowPoint.y);
      coords.push(x2 - lowPoint.x);
      coords.push(y2 - lowPoint.y);
      coords.push(x3 - lowPoint.x);
      coords.push(y3 - lowPoint.y);
      coords.push(x4 - lowPoint.x);
      coords.push(y4 - lowPoint.y);
      coords.push(lowPoint.x)
      coords.push(lowPoint.y)
      return coords;
  }

  calculateCoordinate (radius, angle) {
      console.log("CalculateCoordinates : " + radius + " : " + angle);
      var A = (angle - 90) * (Math.PI/180);
      var x = (radius) * Math.cos(A) ;
      var y = (radius) * Math.sin(A);
      return {x:x, y:y}
  }

  calculateTilePosition (tileNumber, tileCount) {
      var iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
      if (iMax == tileCount) {
          tileCount++;
          iMax = (tileCount%5 ? (tileCount%3 ? (tileCount%2 ? tileCount:tileCount/2) : tileCount/3) : tileCount/5);
          tileCount--;
      }
      var dvMax = Math.ceil(tileCount / iMax)
      var tile = tileCount - 1;

      var width = 145/dvMax;//Math.round((baseX - (extended - delta.x)) / iMax);

      for (var i = 0; i < iMax; i++) {

          for (var dv = 0; dv < dvMax; dv++) {
              if (tile == tileNumber) {
                  return {row:i, col:dv, width:145/dvMax, maxRow:iMax, maxCol:dvMax};
              }
              tile--;
          }
      }
      alert("Tile not found!");
  }
}

export default MultiItemShape;


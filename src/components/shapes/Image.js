
import d3 from 'd3';
import React, {PropTypes} from 'react';

class RectangleShape {

  build(newNodes, domainType) {
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      chgSet.append("image")
          .attr("x", 0)
          .attr("y", 0)
          .attr("width", function(d) { return d.title.width; })
          .attr("height", function(d) { return d.title.height; })
          .attr("xlink:href", function(d) { return d.title.image; });

  }
}

export default RectangleShape;


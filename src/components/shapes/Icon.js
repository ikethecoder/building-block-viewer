
import d3 from 'd3';
import React, {PropTypes} from 'react';

class CircleShape {

  build(newNodes, domainType) {

      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      chgSet.append("circle")
          .attr("r", 30);

      chgSet.append("text")
          .attr("class", "iconText")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", "0.1em")
          .text(function(d) { var hex = d.title.icon.substr(3); return unescape('%u' + hex); });

      chgSet.append("text")
          .attr("class", "labelText")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", "2.0em")
          .text(function(d) { return d.title.label; });

  }
}

export default CircleShape;


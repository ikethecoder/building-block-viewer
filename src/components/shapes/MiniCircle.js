
import d3 from 'd3';
import React, {PropTypes} from 'react';

class CircleShape {

  build(newNodes, domainType) {

      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      chgSet.append("circle")
          .attr("r", 10);

      chgSet.append("text")
          .attr("class", "nodetext")
          .attr("dx", 15)
          .attr("dy", ".35em")
          .text(function(d) { return d.name; });
  }
}

export default CircleShape;


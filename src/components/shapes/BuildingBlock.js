
import d3 from 'd3';
import React, {PropTypes} from 'react';

class RectangleShape {

  build(newNodes, domainType) {
      const chgSet = newNodes.filter(function(d) { return d.type == domainType; });

      chgSet.classed("_" + domainType, true);

      let x;
      let y;
      if (domainType == "buildingBlock") {
        x = 90;
        y = 40;
      } else if (domainType == "stackSolution") {
        x = 60;
        y = 20;
      } else {
        x = 60;
        y = 60;
      }
      chgSet.append("rect")
          .attr("x", -x/2)
          .attr("y", -y/2)
          .attr("width", x)
          .attr("height", y)
          //.style("transform", "translate(-50%, -50%)")
          //.style("filter", "url(#drop-shadow)")

      chgSet.append("text")
          .attr("class", "role")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 15)
          .text(function(d) { return d.title.role; });

      chgSet.append("text")
          .attr("class", "solution")
          .attr("text-anchor", "middle")
          .text(function(d) { return d.title.solution; });

       //   <image x="10" y="20" width="80" height="80" xlink:href="recursion.svg" />
//      chgSet.append("image")
//          .attr("x", -60)
//          .attr("y", -20)
//          .attr("width", 120)
//          .attr("height", 120 * (66/312))
//          .attr("xlink:href", "images/canzea_logo.svg");

  }
}

export default RectangleShape;



import $ from 'jquery';

import d3 from 'd3';
import React, {PropTypes} from 'react';

class Navigation {

  attach (svg) {
     this.svg = svg;

     svg.append("g").attr("class", "nav");
     this.state = { "mode":"closed", "selected":-1, "x":0, "y":0 };
  }

  update () {
      let self = this;
      let vis = this.svg;

      const navData = [
        {"id":"1", "title":"Configuration", "icon":'\uf083'},
        {"id":"2", "title":"Deployment", "icon":'\uf02d '},
        {"id":"3", "title":"Something", "icon":'\uf00a'},
        {"id":"4", "title":"Another", "icon":'\uf045'},
        {"id":"5", "title":"History", "icon":'\uf0c0'}
//        {"id":"6", "title":"History", "icon":'\uf0c0'},
//        {"id":"7", "title":"History", "icon":'\uf0c0'},
//        {"id":"8", "title":"History", "icon":'\uf0c0'},
//        {"id":"9", "title":"History", "icon":'\uf0c0'},
//        {"id":"10", "title":"History", "icon":'\uf0c0'}
      ];

      const navNodes = vis.select("g.nav").selectAll("g")
        .data(navData).enter()
          .append("g")
          .style("visibility", "hidden");

      navNodes.append("rect")
        .attr("fill", "white")
        .attr("x", -35)
        .attr("y", 18)
        .attr("rx", 6)
        .attr("ry", 6)
        .attr("width", "70")
        .attr("height", "12");

      navNodes.append("circle")
        .attr("fill", "black")
        .attr("stroke", "white")
        .attr("stroke-width", "2")
        .attr("r", "20");

      navNodes.append("text")
        .attr("dx", 0)
        .attr("dy", "0")
        .attr("text-anchor", "middle")
        .attr('dominant-baseline', 'central')
        .style('font-family', 'FontAwesome')
        .style('font-size', '20px')
        .style("fill", "white")
        .text(function(d) {return d.icon });

      navNodes.append("text")
        .attr("dx", 0)
        .attr("dy", "2.90em")
        .attr("fill", "black")
        .attr("text-anchor", "middle")
        .text(function(d) { return d.title; });

      navNodes
        .on('click', function (e) {
            console.log("Selected" + e.id);
            self.close();
        })

      navNodes.select("circle")
        .on('mouseenter', function (nodeSet, index) {
            $(this).addClass("nav-hover");
        })
        .on('mouseleave', function (e) {
            $(this).removeClass("nav-hover");
        });
  }

  toggle (e) {
      if (this.state.mode == "closed") {
        this.open(e);
      } else {
        if (this.state.selected != e.index) {
          this.open(e);
        } else {
          this.close(e);
        }
      }
  }

  open (e) {
      this.state.x = e.x;
      this.state.y = e.y;
      this.state.selected = e.index;
      this.state.mode = "open";

      const start = 198;
      let positions = [0];
      for ( let i = 0; i < 10; i++) {
        if ((start+(36*i)) > 360) {
          positions.push(start + (36 * i) - 360);
        } else {
          positions.push(start + (36 * i) );
        }
      }

      let vis = this.svg;

      vis.select("g.nav").selectAll('g')
        .style("opacity", 0)
        .style("visibility", "visible")
        .attr("transform", function(d) {
           return "translate(" + e.x + "," + e.y + ")";
        })
        .transition()
          .style("opacity", 100)
          .attr("transform", function(d) {
           const cx = e.x;
           const cy = e.y + 20;
           const r = 100;
           const a = positions[d.id] * Math.PI / 180;
           const x = cx + r * Math.cos(a)
           const y = cy + r * Math.sin(a)
           return "translate(" + x + "," + y + ")";
        });

  }

  close () {
      let state = this.state;
      let vis = this.svg;

      state.mode = "closed";

      vis.select("g.nav").selectAll('g')
        .transition()
        .style("opacity", 0)
        .attr("transform", function() { return "translate(" + state.x + "," + state.y +")";  } )
        .each("end", function() { vis.select("g.nav").selectAll('g').style("visibility", "hidden") } );
  }
}

export default Navigation;

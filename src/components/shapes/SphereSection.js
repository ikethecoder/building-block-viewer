
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

import twodUtils from './2dUtils'

class SphereSectionShape {

  build(vis, domainType) {

      let obj = vis.append("g");

      obj.attr("transform", "translate(402, 266)")
      obj.classed("_" + domainType, true);


function buildLevel1Solutions(base, size, side, label, delta) {
    var arc = d3.svg.arc()
        .innerRadius(25)
        .outerRadius(35)
        .startAngle(base * (Math.PI/180)) //converting from degs to radians
        .endAngle((base + size) * (Math.PI/180)) //just radians

    obj.append("path")
    .attr("d", arc)
    .attr("stroke", "white")
    .attr("stroke-width", "0.1em")
    .attr("fill", "#CCCCCC")
    .attr("transform", "translate("+delta.x+","+delta.y+")")


    let anchor = "left";
    let rotation = (base - 90 + (size/2));
    //if (base > 180 ) { rotation = rotation - 360; }
    var radius = 30;
    if (side == false) { rotation = 0; radius = 0; anchor = "middle";}
    var A = rotation * (Math.PI/180);
    var x = (radius) * Math.cos(A);
    var y = (radius) * Math.sin(A);

    obj.append("text")
        .attr("class", "title")
        .attr("text-anchor", anchor)
        .attr("dx", 0)
        .attr("dy", 0)
        .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
        .text(function(d) { return label; });


}

function buildLevel1(base, size, side, label, delta) {
    var arc = d3.svg.arc()
        .innerRadius(25)
        .outerRadius(80)
        .startAngle(base * (Math.PI/180)) //converting from degs to radians
        .endAngle((base + size) * (Math.PI/180)) //just radians

    obj.append("path")
    .attr("d", arc)
    .attr("stroke", "white")
    .attr("stroke-width", "0.1em")
    .attr("fill", "lightblue")
    .attr("transform", "translate("+delta.x+","+delta.y+")")


    let anchor = "left";
    let rotation = (base - 90 + (size/2));
    //if (base > 180 ) { rotation = rotation - 360; }
    var radius = 30;
    if (side == false) { rotation = 0; radius = 0; anchor = "middle";}
    var A = rotation * (Math.PI/180);
    var x = (radius) * Math.cos(A);
    var y = (radius) * Math.sin(A);

    obj.append("text")
        .attr("class", "title")
        .attr("text-anchor", anchor)
        .attr("dx", 0)
        .attr("dy", 0)
        .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
        .text(function(d) { return label; });


}


// ENVIRONMENTS
/*
obj.append("circle")
.attr("r", 24)
.attr("stroke", "white")
.attr("stroke-width", "0em")
.attr("fill", "#CCCCCC")
.attr("transform", "translate("+140+","+-100+")")

chgSet.append("text")
    .attr("class", "title")
    .attr("text-anchor", "middle")
    .attr("dx", 140)
    .attr("dy", -100)
    .text(function(d) { return "Environments"; });


//buildLevel1Solutions(230, 70, true, "", {x:140,y:-60});

//buildLevel1(45, 40, true, "Environments", {x:140,y:-60});
buildLevel2(5, 70, 40, ["BUILD","PERF","DEV","QA","LIVE"], 150, {x:140,y:-100}, 25);
buildLevel4(5, 150, 70, 40, true, {x:140,y:-100}, 25);
*/

// RESOURCES
obj.append("circle")
.attr("r", 24)
.attr("stroke", "white")
.attr("stroke-width", "0em")
.attr("fill", "#CCCCCC")
.attr("transform", "translate("+140+","+-30+")")

obj.append("text")
    .attr("class", "title")
    .attr("text-anchor", "middle")
    .attr("dx", 140)
    .attr("dy", -30)
    .text(function(d) { return "Resources"; });

//buildLevel1Solutions(230, 70, true, "", {x:140,y:10});

//buildLevel1(70, 25, true, "Resources", {x:140,y:0});
buildLevel2(3, 50, 30, ["Storage", "Databases", "External"], 150, {x:140,y:-30},25);
buildLevel4(3, 150, 50, 30, true, {x:140,y:-30},25);


// SERVICES
obj.append("circle")
.attr("r", 24)
.attr("stroke", "white")
.attr("stroke-width", "0em")
.attr("fill", "#CCCCCC")
.attr("transform", "translate("+140+","+30+")")

obj.append("text")
    .attr("class", "title")
    .attr("text-anchor", "middle")
    .attr("dx", 140)
    .attr("dy", 30)
    .text(function(d) { return "Applications"; });


//buildLevel1Solutions(230, 70, true, "", {x:140,y:60});

//buildLevel1(90, 20, true, "Internal", {x:140,y:60});
//buildLevel1(110, 20, true, "External", {x:140,y:60});
//buildLevel2(1, 90, 20, [""], 150, {x:140,y:60});
//buildLevel4(1, 150, 90, 20, true, {x:140,y:60});
buildLevel2(2, 100, 30, ["UI Services", "APIs"], 150, {x:140,y:30},25);
buildLevel4(2, 150, 100, 30, true, {x:140,y:30}, 25);


// SOURCE
obj.append("circle")
.attr("r", 24)
.attr("stroke", "white")
.attr("stroke-width", "0em")
.attr("fill", "#CCCCCC")
.attr("transform", "translate("+-80+","+0+")")


buildLevel1(225, 90, false, "Develop", {x:-80,y:0});
obj.append("text")
    .attr("class", "title")
    .attr("text-anchor", "middle")
    .attr("dx", -130)
    .attr("dy", 0)
    .text(function(d) { return "Packages"; });
buildLevel2(4, 225, 90, ["Mobile Stack","React Stack","Java Stack","Configuration"], 160, {x:-80,y:0});
buildLevel4(4, 160, 225, 90, false, {x:-80,y:0});


// App Building Blocks
buildBottomVerticalBase();
buildLevel1(135, 90, false, "", {x:0,y:0});

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 50)
          .text(function(d) { return "App Building Blocks"; });

//buildLevel2Labels(4, 90, 180, ["Network","Security","Data", "Monitoring", ""], 160, {x:0,y:100});
buildLevel4Vertical(4, 125, 90, 180, false, ["Monitoring","Data","Security","Service Mgmt"], {x:0,y:0});


// Continous Delivery Building Blocks
buildTopVerticalBase();
buildLevel1(315, 90, false, "", {x:0,y:0});
//buildLevel2Labels(5, 315, 90, ["Build","Deploy","Test","Secure","Manage"], 160, {x:0,y:0});
//buildLevel4Vertical(4, 180, 315, 90, true, ["","","",""],{x:0,y:0});

function buildLevel2(segments, base, range, _names, outer, delta, inner=82) {


  for ( var x = 0; x < segments; x++) {

    var start = base + (x * range / segments);
    var end = base + ((x+1) * range / segments) - 0.0;
    var arc3 = d3.svg.arc()
        .innerRadius(inner)
        .outerRadius(outer)
        .startAngle(start * (Math.PI/180)) //converting from degs to radians
        .endAngle(end * (Math.PI/180)) //just radians

    obj.append("path")
      .attr("d", arc3)
      .attr("stroke-width", "0.0em")
      .attr("stroke", "white")
      .attr("transform", "translate(" + delta.x+","+delta.y+")")
      .attr("fill", (base == 315 || base == 135 ? "#e2cc93":"#9fcefe"))

  }

  var radius = 90;
  for ( var s = 0; s < segments; s++) {
      let mid = (range/segments) / 2;
      let rotation = (base - 90 + mid + (s * range/segments));// * (Math.PI/180);

      A = rotation * (Math.PI/180);

      if (rotation > 130 && rotation <= 220) {
        rotation = rotation - 180;
        //A = (rotation+180) * (Math.PI/180);
        x = (radius + 50) * Math.cos(A);
        y = (radius + 50) * Math.sin(A);
        const _name = _names[s];
        obj.append("text")
            .attr("class", "title")
            .attr("text-anchor", "left")
            .attr("dx", 0)
            .attr("dy", 0)
            .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
            //.attr("transform", "translate(" + delta.x+","+delta.y+")")
            .text(function(d) { return _name; });
      } else if ((rotation > 45 && rotation <= 130) || (rotation < -45 || rotation > 220)) {
        rotation = 0;
        //A = (rotation+180) * (Math.PI/180);
        x = (radius + 50) * Math.cos(A);
        y = (radius + 50) * Math.sin(A);
        const _name = _names[s];
        obj.append("text")
            .attr("class", "title")
            .attr("text-anchor", "middle")
            .attr("dx", 0)
            .attr("dy", 0)
            .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
            //.attr("transform", "translate(" + delta.x+","+delta.y+")")
            .text(function(d) { return _name; });
      } else {
        x = (radius + 20) * Math.cos(A);
        y = (radius + 20) * Math.sin(A);
        const _name = _names[s];
        obj.append("text")
            .attr("class", "title")
            .attr("text-anchor", "left")
            .attr("dx", 0)
            .attr("dy", 0)
            .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + rotation + ")")
            .text(function(d) { return _name; });
      }

          ///translate(" + x+","+y+")

  }
}

function buildLevel2Labels(segments, base, range, _names, outer, delta, inner=82) {


  var radius = 100;
  for ( var s = 0; s < segments; s++) {
      let mid = (range/segments) / 2;
      let rotation = (base - 90 + mid + (s * range/segments));// * (Math.PI/180);

      A = rotation * (Math.PI/180);
      x = (radius + 20) * Math.cos(A);
      y = (radius + 20) * Math.sin(A);
      const _name = _names[s];
      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "left")
          .attr("dx", 0)
          .attr("dy", 0)
          .attr("transform","translate(" + delta.x+","+delta.y+") translate(" + x+","+y+") rotate(" + 0 + ")")
          .text(function(d) { return _name + rotation; });

          ///translate(" + x+","+y+")

  }
}



function buildLevel4(segments, radius, base, range, side, delta, inner=82) {

  var extended = (side ? "350":"-388");

  for ( var s = 0; s < segments; s++) {

//    var start = base + (s * range / segments);
//    var end = base + ((s+1) * range / segments) - 0.5;
//    var arc3 = d3.svg.arc()
//        .innerRadius(182)
//        .outerRadius(210)
//        .startAngle(start * (Math.PI/180)) //converting from degs to radians
//        .endAngle(end * (Math.PI/180)) //just radians
//
//    obj.append("path")
//      .attr("d", arc3)
//      .attr("fill", "#8cb7e2")

    var A = (base - 90 + (s * range/segments)) * (Math.PI/180);
    var x = (radius) * Math.cos(A);
    var y = (radius) * Math.sin(A);

    var A1 = (base - 90 + ((s+1) * range/segments)) * (Math.PI/180);
    var x1 = (radius) * Math.cos(A1);
    var y1 = (radius) * Math.sin(A1);
    var height = y1-y; //y1-y;//Math.sqrt(Math.pow(s * range/segments, 2)/2);

    //A160,160 0 0,1 -72.63847995832751,-142.56104387013886
    var cx = radius;
    var cy = radius;
    var wid = extended - delta.x;
    obj.append("path")
      .attr("d", "M" + x+","+y+" A" + cx + "," + cy + " 0 0,1 " + (x1) + "," + (y+height) + " L" + wid + "," + (y+height) + " L" + wid+","+y)
      .attr("stroke-width", "0.0em")
      .attr("stroke", "white")
      .attr("fill", "#8cb7e2")
      .attr("transform", "translate("+delta.x+","+delta.y+")")

    }
/*
  for ( s = 0; s < segments; s++) {

      var delt = (side ? 0:-50);
      for ( var t = 0; t < 2; t++) {
        let mid = 4 + (t*3); //(range/segments) / 2;
        let rotation = (base - 90 + mid + (s * range/segments));
        A = rotation * (Math.PI/180);
        x = (radius) * Math.cos(A);
        y = (radius) * Math.sin(A);
        chgSet.append("text")
            .attr("class", "bulletItem")
            .attr("text-anchor", "left")
            .attr("dx", delt + x)
            .attr("dy", y)
            .attr("transform", "translate(10,0) translate("+delta.x+","+delta.y+")")
            .text(function(d) { return "Segment " + (t+1); });
      }
  }
*/
  radius = radius - 20;
  for ( s = 0; s < segments; s++) {

      var start = base + (s * range / segments);
      var end = base + ((x+1) * range / segments);
      A = (base - 90 + (s * range/segments)) * (Math.PI/180);
      x = (radius + 20) * Math.cos(A);
      y = (radius + 20) * Math.sin(A);

      var A0 = (45 - 90 + (s * range/segments)) * (Math.PI/180);
      var x0 = inner * Math.cos(A);
      var y0 = inner * Math.sin(A);

      obj.append("path")
          .attr("d", "M" + x0 + ","+ y0 + "L" + (x) + ","+(y)+" L" + extended+"," + y + "")
          .attr("stroke-width", "0.07em")
          .attr("stroke", "white")
          .attr("fill", "none")
          .attr("transform", "translate("+delta.x+","+delta.y+")")

  }
}




//buildLevel4(4, 210, 315, 90, false, {x:0,y:-20});

//
//  chgSet.filter(function(d) { return d.title.pipeline == true; })
//      .selectAll("arc").data(arcData).enter().append("arc").append("path")
//      .attr("d", arc)
//      .style("fill", "blue");


//      chgSet.filter(function(d) { return d.title.pipeline == true; }).append("path")
//          .attr("d", "M40,40 C(200 200 100 100)")
//          .attr("stroke-width", "0.05em")
//          .attr("stroke", "#333")
//          .attr("fill", "white");


//      chgSet.filter(function(d) { return d.title.pipeline == true; }).append("path")
//          .attr("d", "M0,-22 L-10,-22 L0,3 L-10,28 L120,28 L130,3 L120,-22 L0,-22")
//          .attr("stroke-width", "0.01em")
//          .attr("stroke", "#333")
//          .attr("fill", "white")

      var radius = 25;
      var len = Math.sqrt(Math.pow(radius, 2)/2);
/*

      chgSet.filter(function(d) { return d.title.pipeline == true; }).append("path")
          .attr("d", "M" + len + ","+len+" L200,200 L350,200")
          .attr("stroke-width", "0.01em")
          .attr("stroke", "black")
          .attr("fill", "none")

      chgSet.filter(function(d) { return d.title.pipeline == true; }).append("path")
          .attr("d", "M" + len + ",-"+len+" L160,-160 L350,-160")
          .attr("stroke-width", "0.01em")
          .attr("stroke", "#333")
          .attr("fill", "none")

      chgSet.filter(function(d) { return d.title.pipeline == true; }).append("path")
          .attr("d", "M-" + len + ","+len+" L-200,200 L-350,200")
          .attr("stroke-width", "0.01em")
          .attr("stroke", "#333")
          .attr("fill", "none")

      chgSet.filter(function(d) { return d.title.pipeline == true; }).append("path")
          .attr("d", "M-" + len + ",-"+len+" L-160,-160 L-350,-160")
          .attr("stroke-width", "0.01em")
          .attr("stroke", "#333")
          .attr("fill", "none")
*/


function buildLevel4Vertical(segments, radius, base, range, side, _names, delta) {

  var extended = (side ? -250:250);
  for ( var s = 1; s < segments; s++) {

      var start = base + (s * range / segments);
      A = (base - 90 + (s * range/segments)) * (Math.PI/180);
      x = (radius + 20) * Math.cos(A);
      y = (radius + 20) * Math.sin(A);

      var A0 = (45 - 90 + (s * range/segments)) * (Math.PI/180);
      var x0 = 82 * Math.cos(A);
      var y0 = 82 * Math.sin(A);

      obj.append("path")
          .attr("d", "M" + x0 + ","+ y0 + "L" + (x) + ","+(y)+" L" + x+"," + extended + "")
          .attr("stroke-width", "0.07em")
          .attr("stroke", "white")
          .attr("fill", "none")
          .attr("transform", "translate("+delta.x+","+delta.y+")")
  }

  for ( s = 0; s < segments; s++) {
      start = base + (s * range / segments);
      A = (base - 90 + (s * range/segments)) * (Math.PI/180);
      x = (radius + 20) * Math.cos(A);
      y = (radius + 20) * Math.sin(A);
      y = radius + 20;
      x = x - 20;
      const _name = _names[s];
      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 0)
          .attr("transform", function(d) {
            var c1 = new twodUtils().calculateCoordinate(120, 130 + (s*20));
            return "translate(" + delta.x+","+delta.y+") translate(" + c1.x + ","+ c1.y +") rotate(" + 0 + ")"
          })
          .text(function(d) { return _name; });

  }
}



      radius = 180;
      len = Math.sqrt(Math.pow(radius, 2)/2);

      let A = 0 * (Math.PI/180);
      let x = radius * Math.cos(A);
      let y = radius * Math.sin(A);

//      chgSet.filter(function(d) { return d.title.pipeline == true; }).append("path")
//          .attr("d", "M" + (x) + ","+(y)+" L300," + y + "")
//          .attr("stroke-width", "0.02em")
//          .attr("stroke", "#CCCCCC")
//          .attr("fill", "none")

//      buildLevel3 (2, 90, 25, 300);
//      buildLevel3 (2, 115, 20, 300);
//      buildLevel3 (3, 225, 90, -300);

//      chgSet.append("text")
//          .attr("class", "title")
//          .attr("text-anchor", "left")
//          .attr("dx", 2)
//          .attr("dy", -14)
//          .text(function(d) {
//            displayData (chgSet, d)
//            return d.title.label.toUpperCase(); });
//
//      chgSet.append("line")
//          .attr("x1", 5)
//          .attr("y1", 0)
//          .attr("x2", 110)
//          .attr("y2", 0)
//          .attr("stroke-width", "0.02em")
//          .attr("stroke", "black");
//
//      const cols = [ "QTY", "CORES", "MEMORY" ];
//      $(cols).each(function (d, text) {
//        console.log("A = "+d + ", " + text);
//        chgSet.append("text")
//            .attr("class", "header")
//            .attr("text-anchor", "middle")
//            .attr("dx", 17 + (40 * d))
//            .attr("dy", -2)
//            .text(function(d) { return text; });
//      });


       //   <image x="10" y="20" width="80" height="80" xlink:href="recursion.svg" />
//      chgSet.append("image")
//          .attr("x", -60)
//          .attr("y", -20)
//          .attr("width", 120)
//          .attr("height", 120 * (66/312))
//          .attr("xlink:href", "images/canzea_logo.svg");



      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 0)
          .text(function(d) { return "Ecosystem"; });

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", -60)
          .text(function(d) { return "Continuous Delivery"; });


    function buildTopVerticalBase() {

        x = 0;
        y = 0;
        radius = 500;
        var slant = 100;
        var height = 100;
        var x1 = 290;
        var y1 = 0;
        var cx = radius;
        var cy = radius;
        var delta = {x:-145,y:-170};
        obj.append("path")
          .attr("d", "M" + (x-slant)+","+y+" A" + 0 + "," + 0 + " 0 0,1 " + (x1 + slant) + "," + y + " L" + x1 + "," + (height) + " A" + cx + "," + cy + " 0 0,0 " + (x) + "," + height +" L" + (x-slant) + "," + y + ")")
          .attr("stroke-width", "0.0em")
          .attr("stroke", "white")
          .attr("fill", "#e2cc93")
          .attr("transform", "translate("+delta.x+","+delta.y+")")
    }

    function buildBottomVerticalBase() {

        x = 0;
        y = 0;
        radius = 500;
        var slant = 100;
        var height = 110;
        var x1 = 290;
        var y1 = 0;
        var cx = 500;
        var cy = 500;
        var delta = {x:-145,y:70};
        obj.append("path")
          .attr("d", "M" + (x)+","+y+" A" + radius + "," + radius + " 0 0,0 " + (x1) + "," + y + " L" + (x1+slant) + "," + (height) + " A" + cx + "," + cy + " 0 0,1 " + (x-slant) + "," + height +" L" + (x) + "," + y + ")")
          .attr("stroke-width", "0.0em")
          .attr("stroke", "white")
          .attr("fill", "#e2cc93")
          .attr("transform", "translate("+delta.x+","+delta.y+")")
    }

    buildTopComputeRequirement();

    function buildTopComputeRequirement() {

        x = 0;
        y = 0;
        radius = 0;
        var slant = 0;
        var height = 10;
        var x1 = 600;
        var y1 = 0;
        var cx = radius;
        var cy = radius;
        var delta = {x:-270,y:-230};
        obj.append("path")
          .attr("d", "M" + (x-slant)+","+y+" A" + cx + "," + cy + " 0 0,0 " + (x1 + slant) + "," + y + " L" + x1 + "," + (height) + " A" + 0 + "," + 0 + " 0 0,0 " + (x) + "," + height +" L" + (x-slant) + "," + y + ")")
          .attr("stroke-width", "0.0em")
          .attr("stroke", "white")
          .attr("fill", "#e2cc93")
          .attr("transform", "translate("+delta.x+","+delta.y+")")

        obj.append("text")
            .attr("class", "title")
            .attr("text-anchor", "middle")
            .attr("dx", 0)
            .attr("dy", -222)
            .text(function(d) { return "Environments Compute Requirement"; });

    }

  }
}

export default SphereSectionShape;


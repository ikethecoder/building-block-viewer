
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

import dutil from './2dUtils'

class SectionLeft {

  build(vis, domainType) {
      var twodUtils = new dutil()

      let obj = vis.append("g");

      obj.attr("transform", "translate(402, 266)")
      obj.classed("_" + domainType, true);

      // Continous Delivery Building Blocks
      twodUtils.buildTopVerticalBase(obj);
      //twodUtils.buildLevel1(obj, 315, 90, false, "", {x:0,y:0});

      twodUtils.buildTopComputeRequirement(obj);



  }
}

export default SectionLeft;


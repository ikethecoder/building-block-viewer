
import $ from 'jquery';
import d3 from 'd3';
import React, {PropTypes} from 'react';

import dutil from '../shapes/2dUtils'

class SectionLeft {

  build(vis, domainType) {
      var twodUtils = new dutil()

      let obj = vis.append("g");

      obj.attr("transform", "translate(402, 266)")
      obj.classed("_" + domainType, true);


      obj.append("ellipse")
          .attr("cx", 0)
          .attr("cy", 0)
          .attr("rx", 156)
          .attr("ry", 105)
          .attr("fill", "lightblue")
          .attr("stroke-width", "0.2em")
          .attr("stroke", "white")

      obj.append("ellipse")
          .attr("cx", 0)
          .attr("cy", 0)
          .attr("rx", 156 - 40)
          .attr("ry", 105 - 40)
          .attr("fill", "white")

      obj.append("text")
          .attr("class", "center")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", -5)
          .text(function(d) { return "your"; });

      obj.append("text")
          .attr("class", "center")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 5)
          .text(function(d) { return "ecosystem"; });

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", -85)
          .text(function(d) { return "Continuous"; });

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", -75)
          .text(function(d) { return "Delivery"; });


      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 80)
          .text(function(d) { return "Application"; });

      obj.append("text")
          .attr("class", "title")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 90)
          .text(function(d) { return "Building Blocks"; });

  }
}

export default SectionLeft;

